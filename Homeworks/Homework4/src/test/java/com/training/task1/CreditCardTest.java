package com.training.task1;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class CreditCardTest {

    @Test
    void testTopUpBalance() {
        CreditCard creditCard = new CreditCard("Evgeny", BigDecimal.valueOf(200));
        BigDecimal balance = creditCard.topUpBalance(BigDecimal.valueOf(200));
        assertEquals(BigDecimal.valueOf(400), balance);
    }

    @Test
    void testTopUpBalanceNegative() {
        CreditCard creditCard = new CreditCard("Evgeny", BigDecimal.valueOf(200));
        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> creditCard.topUpBalance(BigDecimal.valueOf(-100))
        );
        assertTrue(thrown.getMessage().contains("Negative number"));
    }

    @Test
    void testWithdraw() {
        CreditCard creditCard = new CreditCard("Evgeny", BigDecimal.valueOf(200));
        BigDecimal balance = creditCard.withdraw(BigDecimal.valueOf(100));
        assertEquals(BigDecimal.valueOf(100), balance);
    }

    @Test
    void testWithdrawBalanceDebt() {
        CreditCard creditCard = new CreditCard("Evgeny", BigDecimal.valueOf(200));
        BigDecimal balance = creditCard.withdraw(BigDecimal.valueOf(300));
        assertEquals(BigDecimal.valueOf(-100), balance);
    }
}