package com.training.task1;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class AtmTest {

    @Test
    void testTopUpBalance() {
        Atm atm = new Atm();
        Card card = new CreditCard("Evgeny", BigDecimal.valueOf(200));
        atm.setCard(card);
        BigDecimal balance = atm.topUpBalance(BigDecimal.valueOf(100));
        assertEquals(BigDecimal.valueOf(300), balance);
    }

    @Test
    void testTopUpBalanceNull() {
        Atm atm = new Atm();
        Card card = new CreditCard("Evgeny", BigDecimal.valueOf(200));
        atm.setCard(card);
        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> atm.topUpBalance(null)
        );
        assertTrue(thrown.getMessage().contains("Cash is null"));
    }

    @Test
    void testWithdraw() {
        Atm atm = new Atm();
        Card card = new CreditCard("Evgeny", BigDecimal.valueOf(200));
        atm.setCard(card);
        BigDecimal balance = atm.withdraw(BigDecimal.valueOf(100));
        assertEquals(BigDecimal.valueOf(100), balance);
    }

    @Test
    void testTopUpBalanceWithoutCard() {
        Atm atm = new Atm();
        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> atm.topUpBalance(BigDecimal.valueOf(200))
        );
        assertTrue(thrown.getMessage().contains("Insert card"));
    }

    @Test
    void testWithdrawNull() {
        Atm atm = new Atm();
        Card card = new CreditCard("Evgeny", BigDecimal.valueOf(200));
        atm.setCard(card);
        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> atm.withdraw(null)
        );
        assertTrue(thrown.getMessage().contains("Cash is null"));
    }

    @Test
    void testWithdrawWithoutCard() {
        Atm atm = new Atm();
        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> atm.withdraw(BigDecimal.valueOf(200))
        );
        assertTrue(thrown.getMessage().contains("Insert card"));
    }

    @Test
    void testConvertCurrency() {
        Atm atm = new Atm();
        Card card = new CreditCard("Evgeny", BigDecimal.valueOf(200));
        atm.setCard(card);
        BigDecimal balance = atm.balanceInCurrencies(DifferentCurrency.USD);
        assertEquals(BigDecimal.valueOf(500.0), balance);
    }
}