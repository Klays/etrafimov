package com.training.task1;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class DebitCardTest {

    @Test
    void testTopUpBalance() {
        DebitCard debitCard = new DebitCard("Evgeny", BigDecimal.valueOf(200));
        BigDecimal balance = debitCard.topUpBalance(BigDecimal.valueOf(200));
        assertEquals(BigDecimal.valueOf(400), balance);
    }

    @Test
    void testTopUpBalanceNegative() {
        DebitCard debitCard = new DebitCard("Evgeny", BigDecimal.valueOf(200));
        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> debitCard.topUpBalance(BigDecimal.valueOf(-100))
        );
        assertTrue(thrown.getMessage().contains("Negative number"));
    }

    @Test
    void testWithdraw() {
        DebitCard debitCard = new DebitCard("Evgeny", BigDecimal.valueOf(200));
        BigDecimal balance = debitCard.withdraw(BigDecimal.valueOf(100));
        assertEquals(BigDecimal.valueOf(100), balance);
    }

    @Test
    void testWithdrawBalanceLessThanRequired() {
        DebitCard debitCard = new DebitCard("Evgeny", BigDecimal.valueOf(100));
        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> debitCard.withdraw(BigDecimal.valueOf(200))
        );
        assertTrue(thrown.getMessage().contains("insufficient funds"));
    }
}