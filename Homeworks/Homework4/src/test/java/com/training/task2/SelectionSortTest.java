package com.training.task2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SelectionSortTest {

    @Test
    void testSelectionSort() {
        SelectionSort selectionSort = new SelectionSort();
        int[] result = selectionSort.sort(new int[]{5, 4, 3, 2, 1});
        assertArrayEquals(new int[]{1, 2, 3, 4, 5}, result);
    }
    @Test
    void testSelectionSortNull() {
        SelectionSort selectionSort = new SelectionSort();
        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> selectionSort.sort(null)
        );
        assertTrue(thrown.getMessage().contains("Array is null"));
    }
}