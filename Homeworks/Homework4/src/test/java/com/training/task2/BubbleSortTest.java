package com.training.task2;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class BubbleSortTest {

    @Test
    void testBubbleSort() {
        BubbleSort bubbleSort = new BubbleSort();
        int[] result = bubbleSort.sort(new int[]{5, 4, 3, 2, 1});
        assertArrayEquals(new int[]{1, 2, 3, 4, 5}, result);
    }

    @Test
    void testBubbleSortNull() {
        BubbleSort bubbleSort = new BubbleSort();
        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> bubbleSort.sort(null)
        );
        assertTrue(thrown.getMessage().contains("Array is null"));
    }

}