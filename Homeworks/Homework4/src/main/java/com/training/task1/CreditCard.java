package com.training.task1;

import java.math.BigDecimal;

public class CreditCard extends Card {

    public CreditCard(String ownerName, BigDecimal balance) {
        super(ownerName, balance);
    }

    @Override
    public BigDecimal withdraw(BigDecimal withdrawCash) {
        if (withdrawCash.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException("Negative number");
        } else {
            return this.balance.subtract(withdrawCash);
        }
    }
}
