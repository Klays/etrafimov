package com.training.task1;

import java.math.BigDecimal;

public class DebitCard extends Card {

    public DebitCard(String ownerName, BigDecimal balance) {
        super(ownerName, balance);
    }

}
