package com.training.task1;

public enum DifferentCurrency {
    USD(2.5D),
    EUR(2.9D),
    BYN(1.0D),
    RYB(29.5D);

    double exchangeRate;

    public double getExchangeRate() {
        return exchangeRate;
    }

    DifferentCurrency(double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public static DifferentCurrency valueOfIgnoreCase(String currency) {
        return DifferentCurrency.valueOf(currency.toUpperCase());
    }
}
