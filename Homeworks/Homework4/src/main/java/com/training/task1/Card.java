package com.training.task1;

import java.math.BigDecimal;

public abstract class Card {

    protected String ownerName;

    protected BigDecimal balance = BigDecimal.valueOf(0);

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    protected Card(String ownerName, BigDecimal balance) {
        this.ownerName = ownerName;
        this.balance = balance;
    }

    protected BigDecimal topUpBalance(BigDecimal cash) {
        if (cash.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException("Negative number");
        } else {
            this.balance = this.balance.add(cash);
            return balance;
        }
    }

    protected BigDecimal withdraw(BigDecimal withdrawCash) {
        if (withdrawCash.compareTo(this.balance) > 0) {
            throw new IllegalArgumentException("insufficient funds");
        } else if (withdrawCash.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException("Negative number");
        } else {
            return this.balance.subtract(withdrawCash);
        }
    }

    protected BigDecimal convert(DifferentCurrency currency) {
        return this.balance.multiply(BigDecimal.valueOf(currency.getExchangeRate()));
    }
}
