package com.training.task1;

import java.math.BigDecimal;

public class Atm {

    private Card card;

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    private void validate(BigDecimal cash) {
        if (card == null) {
            throw new IllegalArgumentException("Insert card");
        } else if (cash == null) {
            throw new IllegalArgumentException("Cash is null");
        }
    }

    public BigDecimal topUpBalance(BigDecimal cash) {
        validate(cash);
        return card.topUpBalance(cash);
    }

    public BigDecimal withdraw(BigDecimal cash) {
        validate(cash);
        return card.withdraw(cash);
    }

    public BigDecimal balanceInCurrencies(DifferentCurrency currency) {
        return card.convert(DifferentCurrency.valueOfIgnoreCase(String.valueOf(currency)));
    }
}
