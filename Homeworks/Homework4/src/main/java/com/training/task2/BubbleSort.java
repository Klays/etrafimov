package com.training.task2;

public class BubbleSort implements Sorter {

    @Override
    public int[] sort(int[] array) {
        if (array == null) {
            throw new IllegalArgumentException("Array is null");
        } else {
            for (int i = 0; i < array.length; i++) {
                int min = array[i];
                int minId = i;
                for (int j = i + 1; j < array.length; j++) {
                    if (array[j] < min) {
                        min = array[j];
                        minId = j;
                    }
                }
                int temp = array[i];
                array[i] = min;
                array[minId] = temp;
            }
        }
        return array;
    }
}
