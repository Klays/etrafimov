package com.training.task2;

import java.util.Random;

public class ArrayCreator {

    public static int[] createArrayWithRandomValues() {
        int[] array = new int[10];
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }
        return array;
    }
}
