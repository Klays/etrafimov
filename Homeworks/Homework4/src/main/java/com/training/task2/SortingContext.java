package com.training.task2;

public class SortingContext {

    private Sorter sortStrategy;

    public Sorter getSortStrategy() {
        return sortStrategy;
    }

    public void setSortStrategy(Sorter sortStrategy) {
        this.sortStrategy = sortStrategy;
    }

    public void execute(int[] array) {
        sortStrategy.sort(array);
       /* if (Sorter.SELECTION.equals(sortStrategy)) {
            Sorter sortSelection = new SelectionSort();
            sortSelection.sort(array);
        } else if (Sorter.BUBBLE.equals(sortStrategy)) {
            Sorter bubbleSort = new BubbleSort();
            bubbleSort.sort(array);
        } else {
            throw new IllegalArgumentException("Wrong input");
        }*/
    }
}
