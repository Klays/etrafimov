package com.training.task2;

public interface Sorter {

    int[] sort(int[] array);

}
