package com.training.task2;

public class SelectionSort implements Sorter {

    @Override
    public int[] sort(int[] array) {
        if (array == null) {
            throw new IllegalArgumentException("Array is null");
        } else {
            for (int i = 0; i < array.length; i++) {
                int position = i;
                int minimum = array[i];
                for (int j = i + 1; j < array.length; j++) {
                    if (array[j] < minimum) {
                        position = j;
                        minimum = array[j];
                    }
                }
                array[position] = array[i];
                array[i] = minimum;
            }
        }
        return array;
    }
}
