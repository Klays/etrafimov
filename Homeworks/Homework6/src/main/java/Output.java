import java.util.Map;

public class Output {
    public void display(Map<String, Map<String, Integer>> map) {

        for (Map.Entry<String, Map<String, Integer>> entry : map.entrySet()) {
            System.out.println(entry.getKey() + ":");
            for (Map.Entry<String, Integer> embeddedEntry : entry.getValue().entrySet()) {
                System.out.println("\t" + embeddedEntry.getKey() + ": " + embeddedEntry.getValue());
            }
        }
    }
}
