import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

public class Sorter {

    public static final String SPACE = " ";

    public Map<String, Map<String, Integer>> sortBySumOfDigits(String text) {
        if (text == null) {
            throw new IllegalArgumentException("string is null");
        }
        String[] words = text.toLowerCase(Locale.ROOT).split(SPACE);

        Map<String, Map<String, Integer>> map = new TreeMap<>();
        for (String word : words) {
            String firstSymb = String.valueOf(word.charAt(0));
            if (map.containsKey(firstSymb)) {
                Map<String, Integer> value = map.get(firstSymb);
                if (value.containsKey(word)) {
                    int count = value.get(word);
                    count++;
                    value.put(word, count);
                } else {
                    value.put(word, 1);
                }
            } else {
                map.put(firstSymb, new TreeMap<>());
                Map<String, Integer> value = map.get(firstSymb);
                value.put(word, 1);
            }
        }
        return map;
    }
}