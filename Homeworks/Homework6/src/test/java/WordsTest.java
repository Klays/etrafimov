import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.*;

class WordsTest {
    @Test
    void testSeparationNull() {
        Sorter words = new Sorter();
        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> words.sortBySumOfDigits(null)
        );
        assertTrue(thrown.getMessage().contains("string is null"));
    }

    @Test
    void testSeparation() {
        Sorter words = new Sorter();

        Map<String, Integer> value = new HashMap<>();
        value.put("aa", 1);

        Map<String, Integer> value1 = new HashMap<>();
        value1.put("bb", 2);

        Map<String, Map<String, Integer>> expected = new TreeMap<>();
        expected.put("a", value);
        expected.put("b", value1);

        Map<String, Map<String, Integer>> result = words.sortBySumOfDigits("BB aa bb");
        assertEquals(expected, result);
    }
}