package com.training.service;

import com.training.model.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductService {

    public static final List<Product> PRODUCTS = initProduct();

    private static List<Product> initProduct() {

        List<Product> products = new ArrayList<>();

        products.add(new Product("Bear", 2));
        products.add(new Product("Book", 5));
        products.add(new Product("Pen", 1));
        products.add(new Product("Notebook", 4));
        products.add(new Product("Cup", 3));
        products.add(new Product("Table", 9));
        products.add(new Product("Pillow", 6));
        products.add(new Product("Carpet", 8));

        return products;
    }
}

