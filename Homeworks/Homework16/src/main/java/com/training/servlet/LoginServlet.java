package com.training.servlet;

import com.training.service.ProductService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("terms") != null) {
            req.setAttribute("products", ProductService.PRODUCTS);
            req.getSession().setAttribute("name", req.getParameter("name"));
            req.getSession().setAttribute("isReadTerms", "yes");
            req.getRequestDispatcher("/productsPage.jsp").forward(req, resp);
        } else {
            resp.sendRedirect("termsErrorPage.jsp");
        }
    }
}
