package com.training.servlet;

import com.training.service.ProductService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/order")
public class OrderServlet extends HttpServlet {

    private static final String USER_PRODUCTS_SESSION = "products";


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String product = req.getParameter("product");
        List<String> products = (List<String>) req.getSession().getAttribute(USER_PRODUCTS_SESSION);

        if (products == null) {
            List<String> userProducts = new ArrayList<>();
            userProducts.add(product);
            req.getSession().setAttribute(USER_PRODUCTS_SESSION, userProducts);
        } else {
            products.add(product);
            req.getSession().setAttribute(USER_PRODUCTS_SESSION, products);
        }
        req.setAttribute(USER_PRODUCTS_SESSION, ProductService.PRODUCTS);
        req.getRequestDispatcher("productsPage.jsp").forward(req, resp);
    }
}
