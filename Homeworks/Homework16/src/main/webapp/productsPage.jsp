<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.ArrayList,com.training.model.Product" %>
<!DOCTYPE html>
<html>
<body>
<div style="text-align: center">
    <form action="${pageContext.request.contextPath}/order" method="post">
        <h1> Hello <%= request.getSession().getAttribute("name")%>!</h1>
        <label> Make your order </label>
        <%
            ArrayList<String> addProducts = (ArrayList<String>) request.getSession().getAttribute("products");
            if (addProducts != null) {

                out.println("<p>You have already chosen: </p>");
                out.println("</ul>");
                for (String product : addProducts) {
                    out.println("<li> " + product + "</li>");
                }
                out.println("</ul>");
            }
        %>
        <br>
        <select name="product">
            <%
                ArrayList<Product> products = (ArrayList<Product>) request.getAttribute("products");
                for (Product product : products) {
                    out.println("<option>" + product.getName() + " (" + product.getPrice() + "$)</option>");
                }
            %>
        </select>
        <br>
        <button type="submit">Add item</button>
        <button type="submit" formaction="${pageContext.request.contextPath}/orderPage.jsp">Submit</button>
    </form>
</div>
</body>
</html>