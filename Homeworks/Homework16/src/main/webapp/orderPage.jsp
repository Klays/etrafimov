<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.ArrayList" %>
<html>
<body>
<div style="text-align: center">
    <h1>
        Dear <%=request.getSession().getAttribute("name")%>, your order:
    </h1>
    <ul>
        <%
            ArrayList<String> things = (ArrayList<String>) request.getSession().getAttribute("products");
            int total = 0;
            for (String thing : things) {
                out.println("<li>" + thing + "</li>");
                total += Integer.parseInt(thing.substring(thing.lastIndexOf('(') + 1, thing.lastIndexOf('$')).trim());
            }
            out.println("<li> Total: $" + total + "</li>");
        %>
    </ul>
</div>
</body>
</html>
