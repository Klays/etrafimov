<%@ page import="com.training.dao.impl.GoodDaoImpl" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.List" %>
<%@ page import="com.training.model.Product" %>
<%@ page import="com.training.dao.impl.GoodsDao" %>
<%@ page import="java.math.BigDecimal" %>
<html>
<body>
<div style="text-align: center">
    <h1>
        Dear <%=request.getSession().getAttribute("name")%>, your order:
    </h1>
    <ul>
        <%
            List<Product> things = GoodsDao.INSTANCE.getUserOrders(String.valueOf(request.getSession().getAttribute("name")));
            BigDecimal sum = BigDecimal.valueOf(0);
            for (Product thing : things) {
                out.println("<li>" + thing.getName() + "(" + thing.getPrice() + "%)</li>");
                sum = sum.add(thing.getPrice());
            }
            out.println("<li> Total: $" + sum + "</li>");
        %>
    </ul>
</div>
</body>
</html>
