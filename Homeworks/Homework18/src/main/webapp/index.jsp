<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
<body>
<div style="text-align: center">
    <h1>Welcome to online shop</h1>
    <br>
    <form action="${pageContext.request.contextPath}/login" method="post">
        <input type="text" name="name" required placeholder="Enter your name">
        <br><br/>
        <input type="checkbox" name="terms"> I agree with the terms of service
        <br><br/>
        <%
            String errorLogin = String.valueOf(request.getAttribute("errorLogin"));
            if (errorLogin != null && errorLogin.equals("no")) {
                out.println("Wrong Name");
            }
        %>
        <button type="submit">Enter</button>
    </form>
</div>
</body>
</html>