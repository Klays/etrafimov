<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.training.model.Product" %>
<!DOCTYPE html>
<html>
<body>
<div style="text-align: center">
    <form action="${pageContext.request.contextPath}/order" method="post">
        <h1> Hello <%= request.getSession().getAttribute("name")%>!</h1>
        <label> Make your order </label>
        <%
            ArrayList<Product> addProducts = (ArrayList<Product>) request.getSession().getAttribute("products");
            if (addProducts != null) {

                out.println("<p>You have already chosen: </p>");
                out.println("</ul>");
                for (Product product : addProducts) {
                    out.println("<li> " + product.getName() + product.getPrice() + " $</li>");
                }
                out.println("</ul>");
            }
        %>
        <br>
        <select name="product">
            <%
                ArrayList<Product> products = (ArrayList<Product>) request.getAttribute("products");
                for (Product product : products) {
                    out.println("<option label=\"" + product.getName() + " (" + product.getPrice() + ")$\">" + product.getId() + "</option>");
                }
            %>
        </select>
        <br>
        <button type="submit">Add item</button>
        <button type="submit" formaction="${pageContext.request.contextPath}/ordersSubmit">Submit</button>
    </form>
</div>
</body>
</html>