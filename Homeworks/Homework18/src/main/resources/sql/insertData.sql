INSERT INTO goods(title, price)
VALUES ('Pen', 1.0);

INSERT INTO goods(title, price)
VALUES ('Table', 99.99);

INSERT INTO goods(title, price)
VALUES ('Mobile Phone', 1499.99);

INSERT INTO goods(title, price)
VALUES ('Doll', 5.2);

INSERT INTO goods(title, price)
VALUES ('Chair', 20.15);

INSERT INTO goods(title, price)
VALUES ('Sweet', 0.50);

INSERT INTO users(id, login, password)
VALUES (1, 'admin', '');

INSERT INTO users(id, login, password)
VALUES (2, 'user', '');

INSERT INTO users(id, login, password)
VALUES (3, 'asd', '');