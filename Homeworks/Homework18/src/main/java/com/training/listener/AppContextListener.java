package com.training.listener;

import com.training.utils.DbManager;
import com.training.utils.DbServerManager;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class AppContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        DbServerManager.start();
        DbManager.initDb();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        DbServerManager.stop();
    }
}
