package com.training.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebFilter("/*")
public class TermsFilter implements Filter {


    private List<String> ignoreList;

    @Override
    public void init(FilterConfig filterConfig) {
        ignoreList = new ArrayList<>();
        ignoreList.add("/login");
        ignoreList.add("/");
        ignoreList.add("/termsErrorPage.jsp");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        if (isURINotContainsInIgnoreList((request).getRequestURI())) {
            String terms = (String) request.getSession().getAttribute("isReadTerms");
            if (terms != null && terms.equalsIgnoreCase("yes")) {
                filterChain.doFilter(servletRequest, servletResponse);
            } else {
                HttpServletResponse response = (HttpServletResponse) servletResponse;
                response.sendRedirect("termsErrorPage.jsp");
            }
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }

    }

    private boolean isURINotContainsInIgnoreList(String uri) {
        return !ignoreList.contains(uri);
    }

    @Override
    public void destroy() {
    }
}

