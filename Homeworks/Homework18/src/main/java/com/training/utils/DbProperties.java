package com.training.utils;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class DbProperties {

    private static final Logger LOGGER = LogManager.getLogger(DbProperties.class);
    private static final String PATH_TO_PROPERTIES = "/db/db.properties";
    private static final DbProperties INSTANCE = new DbProperties();

    private static Properties properties;

    private DbProperties() {

        properties = new Properties();
        try (InputStream fis = DbProperties.class.getResourceAsStream(PATH_TO_PROPERTIES)) {
            properties.load(fis);
        } catch (IOException e) {
            LOGGER.error(e);
        }
    }

    public static String getProperty(String name) {
        return INSTANCE.properties.getProperty(name);
    }
}