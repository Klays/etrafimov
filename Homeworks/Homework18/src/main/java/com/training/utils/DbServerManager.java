package com.training.utils;

import lombok.SneakyThrows;
import org.h2.tools.Server;

import java.sql.SQLException;

public class DbServerManager {

    private static final DbServerManager INSTANCE = new DbServerManager();

    private Server server;

    private DbServerManager() {
        String tcpPort = DbProperties.getProperty("db.tcpPort");
        String[] args = {"-webPort", tcpPort, "-webAllowOthers"};
        try {
            server = Server.createWebServer(args);
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    public static void stop() {
        INSTANCE.getServer().stop();
    }
    @SneakyThrows
    public static void start() { INSTANCE.server.start(); }

    private Server getServer() {
        return server;
    }
}