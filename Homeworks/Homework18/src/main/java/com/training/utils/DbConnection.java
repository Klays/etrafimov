package com.training.utils;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletContextListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnection implements ServletContextListener {

    private DbConnection() {
    }

    private static final Logger LOGGER = LogManager.getLogger(DbConnection.class);

    public static Connection getConnection() {

        String url = DbProperties.getProperty("db.url");
        String username = DbProperties.getProperty("db.login");
        String password = DbProperties.getProperty("db.password");
        String driver = DbProperties.getProperty("db.driver");

        try {
            Class.forName(driver);
            return DriverManager.getConnection(url, username, password);
        } catch (SQLException | ClassNotFoundException e) {
            LOGGER.error("Connection error");
        }
        return null;
    }
}
