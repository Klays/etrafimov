package com.training.utils;

import lombok.SneakyThrows;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.h2.tools.RunScript;

import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Objects;

public class DbManager {

    private static final String SQL_CREATE_PATH = "/sql/createTables.sql";
    private static final String SQL_INSERT_PATH = "/sql/insertData.sql";

    private static final Logger LOGGER = LogManager.getLogger(DbManager.class);

    private DbManager() {
    }

    public static void initDb() {

        try (Connection con = DbConnection.getConnection()) {
            executeScript(con, SQL_CREATE_PATH);
            executeScript(con, SQL_INSERT_PATH);
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    @SneakyThrows
    private static void executeScript(Connection con, String sqlPath) {

        try (InputStreamReader ipr = new InputStreamReader(
                Objects.requireNonNull(DbManager.class.getResourceAsStream(sqlPath)))) {
            RunScript.execute(con, ipr);
        } catch (IOException e) {
            LOGGER.error("Script execution error");
        }
    }
}
