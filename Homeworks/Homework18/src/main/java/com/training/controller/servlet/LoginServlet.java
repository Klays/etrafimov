package com.training.controller.servlet;

import com.training.dao.impl.GoodsDao;
import com.training.dao.impl.UserDao;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger(LoginServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            if (req.getParameter("terms") != null) {
                if (UserDao.INSTANCE.getByLogin(req.getParameter("name")).isPresent()) {
                    req.setAttribute("products", GoodsDao.INSTANCE.getAll());
                    req.getSession().setAttribute("name", req.getParameter("name"));
                    req.getSession().setAttribute("isReadTerms", "yes");
                    req.getRequestDispatcher("/productsPage.jsp").forward(req, resp);
                } else {
                    req.setAttribute("errorLogin", "no");
                    req.getRequestDispatcher("/").forward(req, resp);
                }
            } else {
                resp.sendRedirect("termsErrorPage.jsp");
            }
        } catch (ServletException | IOException ex) {
            LOGGER.error("Page error");
        }
    }
}
