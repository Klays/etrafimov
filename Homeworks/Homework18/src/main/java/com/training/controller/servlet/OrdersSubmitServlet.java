package com.training.controller.servlet;

import com.training.dao.impl.OrdersDao;
import com.training.dao.impl.UserDao;
import com.training.model.Product;
import com.training.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

@WebServlet("/ordersSubmit")
public class OrdersSubmitServlet extends HttpServlet {

    public static final String USER_PRODUCTS_SESSION = "products";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<Product> products = (List<Product>) req.getSession().getAttribute(USER_PRODUCTS_SESSION);
        BigDecimal sum = products.stream().map(Product::getPrice).reduce(BigDecimal.ZERO,BigDecimal::add);

        User user = UserDao.INSTANCE.getByLogin(String.valueOf(req.getSession().getAttribute("name"))).get();

        Long orderId = OrdersDao.INSTANCE.insertOrder(user.getId(), sum);

        products.forEach(el -> OrdersDao.INSTANCE.insertOrderGood(orderId, el.getId()));
        req.getRequestDispatcher("/orderPage.jsp").forward(req, resp);
    }
}
