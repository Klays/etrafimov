package com.training.controller.servlet;

import com.training.dao.impl.GoodsDao;
import com.training.model.Product;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/order")
public class OrderServlet extends HttpServlet {

    public static final String USER_PRODUCTS_SESSION = "products";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String productId = req.getParameter("product");
        List<Product> products = (List<Product>) req.getSession().getAttribute(USER_PRODUCTS_SESSION);
        Product product = GoodsDao.INSTANCE.getById(Long.parseLong(productId));

        if (products == null) {
            List<Product> userProducts = new ArrayList<>();
            userProducts.add(product);
            req.getSession().setAttribute(USER_PRODUCTS_SESSION, userProducts);
        } else {
            products.add(product);
            req.getSession().setAttribute(USER_PRODUCTS_SESSION, products);
        }
        req.setAttribute(USER_PRODUCTS_SESSION, GoodsDao.INSTANCE.getAll());
        req.getRequestDispatcher("productsPage.jsp").forward(req, resp);
    }
}
