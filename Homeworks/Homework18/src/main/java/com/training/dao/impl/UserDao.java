package com.training.dao.impl;

import com.training.dao.BaseDao;
import com.training.model.User;
import com.training.utils.DbConnection;
import lombok.SneakyThrows;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class UserDao implements BaseDao<User, Long> {

    private static final String SQL_SELECT_BY_LOGIN = "SELECT * FROM users WHERE login = ?";
    private static final String SQL_SELECT_ALL = "SELECT * FROM users";

    private UserDao() {
    }

    public static final UserDao INSTANCE = new UserDao();

    @SneakyThrows
    public Optional<User> getByLogin(String login) {
        try (PreparedStatement statement = Objects.requireNonNull(DbConnection.getConnection()).prepareStatement(SQL_SELECT_BY_LOGIN)) {
            statement.setString(1, login);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(parseSet(resultSet));
                }
            }
        }
        return Optional.empty();
    }

    @SneakyThrows
    @Override
    public List<User> getAll() {
        List<User> users = new ArrayList<>();
        try (PreparedStatement statement = Objects.requireNonNull(DbConnection.getConnection()).prepareStatement(SQL_SELECT_ALL)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    users.add(parseSet(resultSet));
                }
            }
        }
        return users;
    }


    private User parseSet(ResultSet resultSet) throws SQLException {
        Long userId = resultSet.getLong(1);
        String login = resultSet.getString(2);
        String password = resultSet.getString(3);
        return new User(userId, login, password);
    }

    @Override
    public User getById(Long id) {
        throw new UnsupportedOperationException();
    }
}
