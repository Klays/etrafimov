package com.training.dao.impl;

import com.training.dao.BaseDao;
import com.training.model.Product;
import com.training.utils.DbConnection;
import lombok.SneakyThrows;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class GoodsDao implements BaseDao<Product, Long> {
    private static final String SQL_SELECT_ALL = "SELECT * FROM goods";
    private static final String SQL_GET_BY_ID = "SELECT * FROM goods WHERE id=?";
    private static final String SQL_SELECT_USER_ORDERS = "select * from goods as g\n" +
            "\tjoin orders_goods as og on g.id = og.good_id\n" +
            "\tjoin orders as o on og.order_id = o.id\n" +
            "\tjoin users as u on o.user_id = u.id\n" +
            "\t\twhere u.login = ?";

    private GoodsDao() {
    }

    public static final GoodsDao INSTANCE = new GoodsDao();

    private void extracted(List<Product> products, PreparedStatement statement) throws SQLException {
        try (ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                products.add(parseSet(resultSet));
            }
        }
    }
    @SneakyThrows
    @Override
    public List<Product> getAll() {
        List<Product> products = new ArrayList<>();
        try (PreparedStatement statement = Objects.requireNonNull(DbConnection.getConnection()).prepareStatement(SQL_SELECT_ALL)) {
            extracted(products, statement);
        }
        return products;
    }



    @SneakyThrows
    public List<Product> getUserOrders(String login) {
        List<Product> products = new ArrayList<>();
        try (PreparedStatement statement = Objects.requireNonNull(DbConnection.getConnection()).prepareStatement(SQL_SELECT_USER_ORDERS)) {
            extracted(products, statement);
        }
        return products;
    }

    private Product parseSet(ResultSet resultSet) throws SQLException {
        Long productId = resultSet.getLong(1);
        String name = resultSet.getString(2);
        BigDecimal price = resultSet.getBigDecimal(3);
        return new Product(productId, name, price);
    }

    @SneakyThrows
    @Override
    public Product getById(Long id) {
        try (PreparedStatement statement = Objects.requireNonNull(DbConnection.getConnection()).prepareStatement(SQL_GET_BY_ID)) {
            statement.setString(1, String.valueOf(id));
            return getProduct(statement);
        }
    }

    private Product getProduct(PreparedStatement statement) throws SQLException {
        try (ResultSet resultSet = statement.executeQuery()) {
            resultSet.next();
            return parseSet(resultSet);
        }
    }
}
