package training;

import com.training.StaticServiceForNewJava;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class StaticServiceForNewJavaTest {

    @Test
    void testCountEvenNumbers() {

        StaticServiceForNewJava staticServiceForNewJava = new StaticServiceForNewJava();

        List<Long> numbers = new ArrayList<>();
        numbers.add(2L);
        numbers.add(3L);
        numbers.add(4L);

        long actual = staticServiceForNewJava.countEvenNumbers(numbers);

        assertEquals(6L, actual);
    }

    @Test
    void testCalculateSquareOfDistinctNumbers() {

        StaticServiceForNewJava staticServiceForNewJava = new StaticServiceForNewJava();

        List<Long> numbers = new ArrayList<>();
        numbers.add(4L);
        numbers.add(3L);
        numbers.add(2L);

        List<Long> expected = new ArrayList<>();
        expected.add(4L);
        expected.add(9L);
        expected.add(16L);

        List<Long> actual = staticServiceForNewJava.calculateSquareOfDistinctNumbers(numbers);

        assertEquals(expected, actual);
    }

    @Test
    void testGroupStringsByLastLetter() {

        StaticServiceForNewJava staticServiceForNewJava = new StaticServiceForNewJava();

        List<String> firstList = new ArrayList<>();
        firstList.add("AB");
        firstList.add("GA");

        List<String> secondList = new ArrayList<>();
        secondList.add("FB");
        secondList.add("IA");

        Set<List<String>> strings = new HashSet<>();
        strings.add(firstList);
        strings.add(secondList);

        List<String> firstListForMap = new ArrayList<>();
        firstListForMap.add("GA");
        firstListForMap.add("IA");

        List<String> secondListForMap = new ArrayList<>();
        secondListForMap.add("AB");
        secondListForMap.add("FB");

        Map<Character, List<String>> expected = new HashMap<>();
        expected.put('A', firstListForMap);
        expected.put('B', secondListForMap);

        Map<Character, List<String>> actual = staticServiceForNewJava.groupStringsByLastLetter(strings);

        assertEquals(expected, actual);

    }

    @Test
    void testNull() {

        StaticServiceForNewJava staticServiceForNewJava = new StaticServiceForNewJava();

        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> staticServiceForNewJava.countEvenNumbers(null)
        );
        assertTrue(thrown.getMessage().contains("List is null"));
    }
}