package com.training;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class StaticServiceForNewJava implements StatisticService {

    public void validate(Object object) {

        if (object == null) {
            throw new IllegalArgumentException("List is null");
        }
    }

    @Override
    public long countEvenNumbers(List<Long> numbers) {

        validate(numbers);
        return IntStream.range(0, numbers.size())
                .filter(i -> numbers.get(i) % 2 == 0)
                .map(i -> Math.toIntExact(numbers.get(i))).sum();
    }

    @Override
    public List<Long> calculateSquareOfDistinctNumbers(List<Long> numbers) {

        validate(numbers);
        return numbers.stream()
                .map(number -> number * number)
                .sorted().toList();

    }

    @Override
    public Map<Character, List<String>> groupStringsByLastLetter(Set<List<String>> strings) {

        validate(strings);
        return strings.stream().flatMap(Collection::stream)
                .collect(Collectors.groupingBy(s -> Character.toUpperCase(s.charAt(s.length() - 1))));

    }
}