package com.training;

import java.util.*;

public class StaticServiceForOldJava implements StatisticService {

    public void validate(Object object) {

        if (object == null) {
            throw new IllegalArgumentException("List is null");
        }
    }

    @Override
    public long countEvenNumbers(List<Long> numbers) {

        validate(numbers);
        int sum = 0;
        for (Long number : numbers) {
            if (number % 2 == 0) {
                sum += number;
            }
        }
        return sum;
    }

    @Override
    public List<Long> calculateSquareOfDistinctNumbers(List<Long> numbers) {

        validate(numbers);
        List<Long> squareNumbers = new ArrayList<>();
        for (Long number : numbers) {
            squareNumbers.add(number * number);
        }
        Collections.sort(squareNumbers);
        return squareNumbers;
    }

    @Override
    public Map<Character, List<String>> groupStringsByLastLetter(Set<List<String>> strings) {

        validate(strings);

        Map<Character, List<String>> map = new HashMap<>();

        for (List<String> string : strings) {
            for (String word : string) {
                Character lastCharacter = word.charAt(word.length() - 1);
                if (map.containsKey(Character.toUpperCase(lastCharacter))) {
                    List<String> digit = map.get(lastCharacter);
                    digit.add(word);
                } else {
                    map.put(lastCharacter, new ArrayList<>());
                    List<String> digit = map.get(lastCharacter);
                    digit.add(word);
                }
            }
        }
        return map;
    }
}
