package com.training.task2;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TeamChooserTest {

    @Test
    void testChooseTeam() {

        Worker firstWorker = new Worker();
        firstWorker.getSkillMap().put(Skill.ARCHITECT, 2);
        Worker secondWorker = new Worker();
        secondWorker.getSkillMap().put(Skill.WELDER, 2);
        Worker thirdWorker = new Worker();
        thirdWorker.getSkillMap().put(Skill.LOCKSMITH, 2);

        Worker fourthWorker = new Worker();
        fourthWorker.getSkillMap().put(Skill.ARCHITECT, 3);
        Worker fifthWorker = new Worker();
        fifthWorker.getSkillMap().put(Skill.WELDER, 3);
        Worker sixthWorker = new Worker();
        sixthWorker.getSkillMap().put(Skill.LOCKSMITH, 3);

        Worker seventhWorker = new Worker();
        seventhWorker.getSkillMap().put(Skill.ARCHITECT, 1);
        Worker eighthWorker = new Worker();
        eighthWorker.getSkillMap().put(Skill.WELDER, 1);
        Worker ninthWorker = new Worker();
        ninthWorker.getSkillMap().put(Skill.LOCKSMITH, 1);

        Team firstTeam = new Team();
        firstTeam.getWorkers().addAll(List.of(firstWorker, secondWorker, thirdWorker));

        Team secondTeam = new Team();
        secondTeam.getWorkers().addAll(List.of(fourthWorker, fifthWorker, sixthWorker));

        Team thirdTeam = new Team();
        thirdTeam.getWorkers().addAll(List.of(seventhWorker, eighthWorker, ninthWorker));

        Team team = TeamChooser.chooseTeam(new Tender(10, List.of(Skill.ARCHITECT, Skill.WELDER, Skill.LOCKSMITH)),
                List.of(firstTeam, secondTeam, thirdTeam));

        assertEquals(thirdTeam, team);
    }

    @Test
    void testChooseTeamNotEnoughBudget() {

        Worker firstWorker = new Worker();
        firstWorker.getSkillMap().put(Skill.ARCHITECT, 5);
        Worker secondWorker = new Worker();
        secondWorker.getSkillMap().put(Skill.WELDER, 5);
        Worker thirdWorker = new Worker();
        thirdWorker.getSkillMap().put(Skill.LOCKSMITH, 5);

        Worker fourthWorker = new Worker();
        fourthWorker.getSkillMap().put(Skill.ARCHITECT, 5);
        Worker fifthWorker = new Worker();
        fifthWorker.getSkillMap().put(Skill.WELDER, 5);
        Worker sixthWorker = new Worker();
        sixthWorker.getSkillMap().put(Skill.LOCKSMITH, 5);

        Worker seventhWorker = new Worker();
        seventhWorker.getSkillMap().put(Skill.ARCHITECT, 5);
        Worker eighthWorker = new Worker();
        eighthWorker.getSkillMap().put(Skill.WELDER, 5);
        Worker ninthWorker = new Worker();
        ninthWorker.getSkillMap().put(Skill.LOCKSMITH, 5);

        Team firstTeam = new Team();
        firstTeam.getWorkers().addAll(List.of(firstWorker, secondWorker, thirdWorker));

        Team secondTeam = new Team();
        secondTeam.getWorkers().addAll(List.of(fourthWorker, fifthWorker, sixthWorker));

        Team thirdTeam = new Team();
        thirdTeam.getWorkers().addAll(List.of(seventhWorker, eighthWorker, ninthWorker));

        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> TeamChooser.chooseTeam(new Tender(10, List.of(Skill.ARCHITECT, Skill.WELDER, Skill.LOCKSMITH)), List.of(firstTeam, secondTeam, thirdTeam))
        );
        assertTrue(thrown.getMessage().contains("Not enough budget"));
    }

    @Test
    void testChooseTeamNoRequiredSkills() {

        Worker firstWorker = new Worker();
        firstWorker.getSkillMap().put(Skill.ARCHITECT, 5);
        Worker secondWorker = new Worker();
        secondWorker.getSkillMap().put(Skill.WELDER, 5);
        Worker thirdWorker = new Worker();
        thirdWorker.getSkillMap().put(Skill.LOCKSMITH, 5);

        Worker fourthWorker = new Worker();
        fourthWorker.getSkillMap().put(Skill.ARCHITECT, 5);
        Worker fifthWorker = new Worker();
        fifthWorker.getSkillMap().put(Skill.WELDER, 5);
        Worker sixthWorker = new Worker();
        sixthWorker.getSkillMap().put(Skill.LOCKSMITH, 5);

        Worker seventhWorker = new Worker();
        seventhWorker.getSkillMap().put(Skill.ARCHITECT, 5);
        Worker eighthWorker = new Worker();
        eighthWorker.getSkillMap().put(Skill.WELDER, 5);
        Worker ninthWorker = new Worker();
        ninthWorker.getSkillMap().put(Skill.LOCKSMITH, 5);

        Team firstTeam = new Team();
        firstTeam.getWorkers().addAll(List.of(firstWorker, secondWorker, thirdWorker));

        Team secondTeam = new Team();
        secondTeam.getWorkers().addAll(List.of(fourthWorker, fifthWorker, sixthWorker));

        Team thirdTeam = new Team();
        thirdTeam.getWorkers().addAll(List.of(seventhWorker, eighthWorker, ninthWorker));

        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> TeamChooser.chooseTeam(new Tender(10, List.of(Skill.ARCHITECT, Skill.WELDER, Skill.ELECTRICIAN)),
                        List.of(firstTeam, secondTeam, thirdTeam))
        );
        assertTrue(thrown.getMessage().contains("Groups with required skills not found"));
    }
}