package com.training.task1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyArrayListTest {

    @Test
    void testFreeCells() {
        MyArrayList<String> myArrayList = new MyArrayList<>();
        myArrayList.add("A");
        myArrayList.add("B");

        assertTrue(myArrayList.freeSpace());
    }

    @Test
    void testAdd() {

        MyArrayList<String> myArrayList = new MyArrayList<>();
        myArrayList.add("A");
        myArrayList.add("B");

        assertEquals("A", myArrayList.get(0));
        assertEquals("B", myArrayList.get(1));
    }

    @Test
    void testAddValidate() {

        MyArrayList<String> myArrayList = new MyArrayList<>();

        myArrayList.add("A");
        myArrayList.add("B");
        myArrayList.add("B");
        myArrayList.add("B");
        myArrayList.add("B");
        myArrayList.add("B");
        myArrayList.add("B");
        myArrayList.add("B");
        myArrayList.add("B");
        myArrayList.add("B");

        ValidateSizeException thrown = assertThrows(
                ValidateSizeException.class,
                () -> {
                    myArrayList.add("B");
                });
        assertTrue(thrown.getMessage().contains("Oversize List"));
    }

    @Test
    void testSize() {

        MyArrayList<String> myArrayList = new MyArrayList<>();
        myArrayList.add("A");
        myArrayList.add("B");

        assertEquals(2, myArrayList.size());
    }

    @Test
    void testGetCheckByIndex() {

        MyArrayList<String> myArrayList = new MyArrayList<>();
        myArrayList.add("A");
        myArrayList.add("B");

        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> {
                    myArrayList.get(3);
                });
        assertTrue(thrown.getMessage().contains("Index exceeds the size of List"));
    }

    @Test
    void testGet() {

        MyArrayList<String> myArrayList = new MyArrayList<>();
        myArrayList.add("A");

        assertEquals("A", myArrayList.get(0));
    }

    @Test
    void testIsEmpty() {
        MyArrayList<String> myArrayList = new MyArrayList<>();
        myArrayList.add("A");

        assertFalse(myArrayList.isEmpty());
    }

    @Test
    void testIndexOf() {
        MyArrayList<String> myArrayList = new MyArrayList<>();
        myArrayList.add("A");

        assertEquals(0, myArrayList.indexOf("A"));
    }

    @Test
    void testIndexOfNull() {
        MyArrayList<String> myArrayList = new MyArrayList<>();
        myArrayList.add("A");

        assertEquals(-1, myArrayList.indexOf(null));
    }

    @Test
    void testContains() {
        MyArrayList<String> myArrayList = new MyArrayList<>();
        myArrayList.add("A");

        assertTrue(myArrayList.contains("A"));
    }

    @Test
    void testAddIndex() {

        MyArrayList<String> myArrayList = new MyArrayList<>();
        myArrayList.add("C");
        myArrayList.add("D");
        myArrayList.add("G");
        myArrayList.add(1, "A");
        myArrayList.add(2, "B");

        assertEquals("C", myArrayList.get(0));
        assertEquals("A", myArrayList.get(1));
        assertEquals("B", myArrayList.get(2));
        assertEquals("G", myArrayList.get(3));
    }

    @Test
    void testRemoveObject() {

        MyArrayList<String> myArrayList = new MyArrayList<>();
        myArrayList.add("A");
        myArrayList.add("B");
        myArrayList.add("C");
        myArrayList.remove("B");

        assertEquals(2, myArrayList.size());
        assertEquals("C", myArrayList.get(1));
    }

    @Test
    void testRemoveIndex() {

        MyArrayList<String> myArrayList = new MyArrayList<>();
        myArrayList.add("A");
        myArrayList.add("B");
        myArrayList.add("C");
        myArrayList.remove(1);

        assertEquals(2, myArrayList.size());
        assertEquals("C", myArrayList.get(1));
    }
}