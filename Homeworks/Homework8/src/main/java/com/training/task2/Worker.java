package com.training.task2;

import java.util.HashMap;
import java.util.Map;

public class Worker {

    private Map<Skill, Integer> skillMap = new HashMap<>();

    public Map<Skill, Integer> getSkillMap() {
        return skillMap;
    }

    public void setSkillMap(Map<Skill, Integer> skillMap) {
        this.skillMap = skillMap;
    }

}
