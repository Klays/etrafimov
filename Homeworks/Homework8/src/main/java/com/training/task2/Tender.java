package com.training.task2;

import java.util.List;

public class Tender {

    private Integer budget;
    private List<Skill> requiredSkills;

    public Tender(Integer budget, List<Skill> requiredSkills) {
        this.budget = budget;
        this.requiredSkills = requiredSkills;
    }

    public Integer getBudget() {
        return budget;
    }

    public void setBudget(Integer budget) {
        this.budget = budget;
    }

    public List<Skill> getRequiredSkills() {
        return requiredSkills;
    }

    public void setRequiredSkills(List<Skill> requiredSkills) {
        this.requiredSkills = requiredSkills;
    }
}
