package com.training.task2;

public enum Skill {

    WOODWORKER,
    LOCKSMITH,
    WELDER,
    ARCHITECT,
    ELECTRICIAN,
}
