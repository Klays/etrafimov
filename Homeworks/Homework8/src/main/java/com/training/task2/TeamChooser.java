package com.training.task2;

import java.util.*;
import java.util.stream.Collectors;

public class TeamChooser {

    public static Team chooseTeam(Tender tender, List<Team> teams) {

        Map<Integer, Team> costTeamMap = new TreeMap<>();
        for (Team team : teams) {
            var allSkills = team.getWorkers()
                    .stream()
                    .flatMap(el -> el.getSkillMap().keySet().stream())
                    .collect(Collectors.toList());
            if (allSkills.containsAll(tender.getRequiredSkills())) {
                int totalCost = 0;
                for (Skill skill : tender.getRequiredSkills()) {
                    List<Integer> skillCosts = new ArrayList<>();
                    for (Worker worker : team.getWorkers()) {
                        for (Map.Entry<Skill, Integer> entry : worker.getSkillMap().entrySet()) {
                            if (entry.getKey().equals(skill)) {
                                skillCosts.add(entry.getValue());
                            }
                        }
                    }
                    totalCost += skillCosts.stream().min(Comparator.naturalOrder()).get();
                }
                costTeamMap.put(totalCost, team);
            }
        }

        Optional<Map.Entry<Integer, Team>> firstTeam = costTeamMap.entrySet().stream().findFirst();

        if (firstTeam.isPresent()) {
            if (firstTeam.get().getKey() > tender.getBudget()) {
                throw new IllegalArgumentException("Not enough budget");
            } else {
                return firstTeam.get().getValue();
            }
        } else {
            throw new IllegalArgumentException("Groups with required skills not found");
        }
    }
}
