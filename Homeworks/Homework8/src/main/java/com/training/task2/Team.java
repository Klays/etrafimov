package com.training.task2;

import java.util.ArrayList;
import java.util.List;

public class Team {

    private List<Worker> workers = new ArrayList<>();

    public List<Worker> getWorkers() {
        return workers;
    }

    public void setWorkers(List<Worker> workers) {
        this.workers = workers;
    }

}
