package com.training.task1;

public class MyArrayList<T> implements MyList<T> {

    private final T[] elementData;

    public MyArrayList() {
        elementData = (T[]) new Object[limitSize];
    }

    private final int limitSize = 10;

    private int size;

    @Override
    public boolean freeSpace() {
        for (int i = 0; i < elementData.length; i++) {
            if (elementData[i] == null) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void chekIndex(int index) {

        if (index > size) {
            throw new IllegalArgumentException("Index exceeds the size of List");
        }
    }

    @Override
    public void chekSize(int size) {

        if (size >= limitSize) {
            throw new ValidateSizeException("Oversize List");
        }
    }

    @Override
    public boolean isEmpty() {

        return size == 0;
    }

    @Override
    public int size() {

        return size;
    }

    @Override
    public void clear() {

        Object[] objects = new Object[limitSize];

    }

    @Override
    public T get(int index) {

        chekIndex(index);
        if (elementData[index] == null) {
            throw new IllegalArgumentException("Item not found");
        }
        return elementData[index];
    }

    @Override
    public int indexOf(T t) {

        if (t == null) {
            for (int i = 0; i < size; i++)
                if (elementData[i] == null)
                    return i;
        } else {
            for (int i = 0; i < size; i++)
                if (t.equals(elementData[i])) {
                    return i;
                }
        }
        return -1;
    }

    @Override
    public boolean contains(T t) {

        final T[] objects = elementData;
        for (T object : objects) {
            if (t.equals(object)) {
                return true;
            }
        }
        return false;
    }


    @Override
    public void add(T t) {

        chekSize(size);
        elementData[size] = t;
        size++;
    }

    @Override
    public void remove(int index) {

        chekIndex(index);
        for (int i = 0; i < elementData.length; i++) {
            if (i == index) {
                size--;
                elementData[i] = null;
                if (elementData.length - 1 - i >= 0)
                    System.arraycopy(elementData, i + 1,
                            elementData, i, elementData.length - 1 - i);
                break;
            }
        }
    }

    @Override
    public void remove(T t) {

        for (int i = 0; i < elementData.length; i++) {
            if (t.equals(elementData[i])) {
                size--;
                elementData[i] = null;
                if (elementData.length - 1 - i >= 0)
                    System.arraycopy(elementData, i + 1,
                            elementData, i, elementData.length - 1 - i);
                break;
            }
        }
    }


    @Override
    public void add(int index, T t) {

        chekIndex(index);
        chekSize(size);
        for (int i = 0; i < size; i++) {
            if (index == i) {
                size++;
                elementData[i] = t;
                if (size - (i + 1) >= 0) System.arraycopy(elementData, i + 1,
                        elementData, i + 1 + 1, size - (i + 1));
            }
        }
    }
}
