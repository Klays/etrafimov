package com.training.task1;

public class ValidateSizeException extends RuntimeException {

    public ValidateSizeException(String string) {
        super(string);
    }
}
