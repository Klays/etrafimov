package com.training.task1;

public interface MyList<T> {

    public boolean isEmpty();

    public int size();

    public void clear();

    public Object get(int index);

    public int indexOf(T t);

    public boolean contains(T t);

    public void add(T t);

    public void add(int index, T t);

    public void remove(int index);

    public void remove(T t);

    public void chekSize(int size);

    public void chekIndex(int index);

    public boolean freeSpace();
}
