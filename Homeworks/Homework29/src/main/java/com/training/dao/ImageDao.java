package com.training.dao;

import com.training.model.Image;

import java.util.List;

public interface ImageDao {

    Image save(Image image);

    List<Image> findAllImages();

    Image getById(Long id);
}
