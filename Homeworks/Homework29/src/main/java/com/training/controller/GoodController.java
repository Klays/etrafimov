package com.training.controller;

import com.training.model.Good;
import com.training.service.GoodService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/goods")
@RequiredArgsConstructor
public class GoodController {

    private final GoodService goodService;

    @PostMapping
    public ResponseEntity<Good> save(@Valid @RequestBody Good good) {
        Good newGood = goodService.save(good);
        return ResponseEntity.status(HttpStatus.CREATED).body(newGood);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Good> findById(@PathVariable Long id) {
        Good good = goodService.findById(id);
        return ResponseEntity.ok(good);
    }

    @GetMapping
    public ResponseEntity<List<Good>> findAll() {
        List<Good> category = goodService.findAll();
        return ResponseEntity.ok(category);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Good> update(@PathVariable Long id, @RequestBody Good good) {
        Good newGood = goodService.update(id, good);
        return ResponseEntity.ok(newGood);
    }
}

