package com.training.controller;

import com.training.Dto.OrderDto;
import com.training.model.Order;
import com.training.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/orders")
public class OrderController {

    private final OrderService orderService;

    @SneakyThrows
    @PostMapping
    public ResponseEntity<Order> sendEmail(@Valid @RequestBody OrderDto orderDto) {
        Order order = orderService.create(orderDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(order);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Order> update(@PathVariable Long id, @RequestBody Order order) {
        if (order.getId().equals(id)) {
            Order newOrder = orderService.update(id, order);
            return ResponseEntity.ok(newOrder);
        }
        throw new IllegalArgumentException("Invalid id");
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Order> findById(@PathVariable Long id) {

        Order order = orderService.findById(id);
        return ResponseEntity.ok(order);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable Long id) {
        orderService.delete(id);
    }

}
