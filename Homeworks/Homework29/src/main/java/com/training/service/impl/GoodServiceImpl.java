package com.training.service.impl;

import com.training.dao.GoodsDao;
import com.training.model.Good;
import com.training.service.GoodService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Service
@RequiredArgsConstructor
public class GoodServiceImpl implements GoodService {

    private final GoodsDao goodsDao;

    @Override
    @Transactional
    public Good update(Long id, Good good) {
        return goodsDao.update(id, good);
    }

    @Override
    @Transactional
    public Good save(Good good) {
        return goodsDao.save(good);
    }

    @Override
    @Transactional()
    public List<Good> findAll() {
        return goodsDao.getAll();
    }

    @Override
    @Transactional()
    public Good findById(Long id) {
        return goodsDao.getById(id);
    }

    @Override
    @Transactional()
    public BigDecimal getTotalPrice(List<Good> goods) {

        if (goods != null) {
            return goods.stream().map(Good::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add);
        } else {
            return BigDecimal.ZERO;
        }
    }

}
