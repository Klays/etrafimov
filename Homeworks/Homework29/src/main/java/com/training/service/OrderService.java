package com.training.service;

import com.training.Dto.OrderDto;
import com.training.model.Order;

import java.util.List;

public interface OrderService {

    Order update(Long id, Order order);

    Order save(Order order);

    List<Order> findAll();

    Order findById(Long id);

    void delete(Long id);

    Order create(OrderDto orderDto);
}
