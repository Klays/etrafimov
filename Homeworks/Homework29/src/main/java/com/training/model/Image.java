package com.training.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Image {

    @Id
    @GeneratedValue
    public Long id;

    public String name;

    @Lob
    @Column(columnDefinition = "BLOB")
    public byte[] image;


}
