package com.training.dao;

import com.training.model.Order;

public interface OrdersDao {

    void saveOrder(Order order);
}
