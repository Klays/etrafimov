package com.training.dao.impl;

import com.training.dao.GoodsDao;
import com.training.exceptions.NotFoundException;
import com.training.model.Good;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Slf4j
@Repository
@RequiredArgsConstructor
public class GoodsDaoImpl implements GoodsDao {

    private static final String GET_GOOD_QUERY = "FROM Good g WHERE g.id = :id";

    private static final String SELECT_ALL = "FROM Good";

    private static final String SELECT_USER_ORDERS = "SELECT id, title, price FROM Good g" +
            "inner join orders_goods og " +
            "inner join orders o " +
            "inner join users u " +
            "where u.login = :login";

    private final SessionFactory sessionFactory;

    @Override
    public List<Good> getAll() {
        return sessionFactory.getCurrentSession()
                .createQuery(SELECT_ALL, Good.class).getResultList();
    }

    @Override
    public List<Good> getUserOrders(String login) {
        if (login == null) {
            throw new NotFoundException("User is not found");
        }
        return sessionFactory.getCurrentSession()
                .createQuery(SELECT_USER_ORDERS, Good.class)
                .setParameter("login", login).getResultList();
    }

    @Override
    public Good getById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Invalid id");
        }
        return sessionFactory.getCurrentSession()
                .createQuery(GET_GOOD_QUERY, Good.class)
                .setParameter("id", id)
                .uniqueResult();

    }
}
