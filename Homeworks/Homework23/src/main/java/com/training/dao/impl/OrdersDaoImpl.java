package com.training.dao.impl;

import com.training.dao.OrdersDao;
import com.training.model.Order;
import lombok.RequiredArgsConstructor;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class OrdersDaoImpl implements OrdersDao {

    private final SessionFactory sessionFactory;

    @Override
    public void saveOrder(Order order) {
        sessionFactory.getCurrentSession().save(order);
    }
}
