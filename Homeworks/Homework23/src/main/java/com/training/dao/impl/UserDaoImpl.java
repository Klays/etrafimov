package com.training.dao.impl;

import com.training.dao.UserDao;
import com.training.exceptions.NotFoundException;
import com.training.model.User;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Slf4j
@Repository
@RequiredArgsConstructor
public class UserDaoImpl implements UserDao {

    private static final String SELECT_BY_LOGIN = "FROM User u WHERE u.login = :login";

    private static final String GET_USER_BY_ID_QUERY = "FROM User u WHERE u.id = :id";

    private static final String SELECT_ALL = "FROM User";

    private final SessionFactory sessionFactory;

    @SneakyThrows
    @Override
    public Optional<User> getByLogin(String login) {
        if (login == null) {
            throw new NotFoundException("User is not found");
        }
        User user = sessionFactory.getCurrentSession().createQuery(SELECT_BY_LOGIN, User.class)
                .setParameter("login", login)
                .uniqueResult();

        return Optional.ofNullable(user);
    }

    @Override
    public void save(User user) {
        sessionFactory.getCurrentSession().save(user);
    }

    @Override
    public List<User> getAll() {
        return sessionFactory.getCurrentSession().createQuery(SELECT_ALL, User.class)
                .getResultList();
    }

    @Override
    public User getById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Invalid user id");
        }
        return sessionFactory.getCurrentSession()
                .createQuery(GET_USER_BY_ID_QUERY, User.class).uniqueResult();
    }

}
