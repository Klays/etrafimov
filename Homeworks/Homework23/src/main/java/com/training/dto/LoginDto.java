package com.training.dto;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class LoginDto {

    private String login;

    private String password;

    private String passwordConfirm;
}
