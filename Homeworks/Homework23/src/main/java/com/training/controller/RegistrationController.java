package com.training.controller;

import com.training.dto.LoginDto;
import com.training.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Scope("session")
@Slf4j
@Controller
@RequiredArgsConstructor
public class RegistrationController {

    private final UserService userService;

    @GetMapping("/registration")
    public ModelAndView registration() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("loginDto", new LoginDto());
        modelAndView.setViewName("registration");
        return modelAndView;
    }

    @SneakyThrows
    @PostMapping("/registration")
    public String processForm(@Valid @ModelAttribute final LoginDto loginDto,
                              final BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            log.info("This user already exists");
        }

        if (loginDto != null) {
            userService.registerNewAccount(loginDto);
        }

        return "redirect:/login";
    }

}
