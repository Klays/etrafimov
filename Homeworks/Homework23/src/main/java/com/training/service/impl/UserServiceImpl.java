package com.training.service.impl;

import com.training.dto.LoginDto;
import com.training.dao.UserDao;
import com.training.exceptions.UserExistsException;
import com.training.model.User;
import com.training.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    @Transactional()
    public Optional<User> findByLogin(String login) {
        return userDao.getByLogin(login);
    }

    @Override
    @Transactional()
    public Optional<String> login(String login) {
        if (userDao.getByLogin(login).isPresent()) {
            return Optional.ofNullable(login);
        } else {
            return Optional.empty();
        }
    }

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public void registerNewAccount(final LoginDto loginDto) {
        if (loginDto.getLogin().isEmpty()) {
            throw new UserExistsException("This user already exists");
        }
        final User user = new User();
        user.setLogin(loginDto.getLogin());
        user.setPassword(passwordEncoder.encode(loginDto.getPassword()));
        userDao.save(user);
    }

}
