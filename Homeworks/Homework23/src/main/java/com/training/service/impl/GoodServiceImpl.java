package com.training.service.impl;

import com.training.dto.OrderDto;
import com.training.dao.GoodsDao;
import com.training.model.Good;
import com.training.service.GoodService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class GoodServiceImpl implements GoodService {

    @Autowired
    private final GoodsDao goodsDao;

    @Override
    @Transactional()
    public List<Good> findAll() {
        return goodsDao.getAll();
    }

    @Override
    @Transactional()
    public Optional<Good> findById(Long id) {
        return Optional.ofNullable(goodsDao.getById(id));
    }

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public List<Good> addGoodToCart(OrderDto orderDto, List<Good> goods) {
        if (goods == null) {
            goods = new ArrayList<>();
        }
        Optional<Good> good = findById(orderDto.getGoodId());
        if (good.isPresent()) {
            goods.add(good.get());
        }
        return goods;
    }

    @Override
    @Transactional()
    public BigDecimal getTotalPrice(List<Good> goods) {

        if (goods != null) {
            return goods.stream().map(Good::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add);
        } else {
            return BigDecimal.ZERO;
        }
    }

    @Override
    @Transactional()
    public List<Good> getUserOrders(String login) {
        return goodsDao.getUserOrders(login);
    }
}
