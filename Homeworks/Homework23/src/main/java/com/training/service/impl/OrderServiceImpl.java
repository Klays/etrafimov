package com.training.service.impl;

import com.training.dao.OrdersDao;
import com.training.model.Good;
import com.training.model.Order;
import com.training.model.User;
import com.training.service.GoodService;
import com.training.service.OrderService;
import com.training.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService {

    private final OrdersDao ordersDao;
    private final UserService userService;
    private final GoodService goodService;

    public OrderServiceImpl(OrdersDao ordersDao, UserService userService, GoodService goodService) {
        this.ordersDao = ordersDao;
        this.userService = userService;
        this.goodService = goodService;
    }

    @Override
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public void submitOrder(String login, List<Good> goods) {
        Optional<User> user = userService.findByLogin(login);
        Order order = new Order();
        order.setTotalPrice(goodService.getTotalPrice(goods));
        order.setUser(user.get());
        order.setGoods(goods);
        ordersDao.saveOrder(order);
    }

}
