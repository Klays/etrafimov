package com.training.service;

import com.training.model.Good;

import java.util.List;

public interface OrderService {

    void submitOrder(String login, List<Good> goods);

}
