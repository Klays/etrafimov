package com.training.service;


import com.training.dto.OrderDto;
import com.training.model.Good;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface GoodService {

    List<Good> findAll();

    Optional<Good> findById(Long id);

    List<Good> addGoodToCart(OrderDto orderDto, List<Good> goods);

    BigDecimal getTotalPrice(List<Good> goods);

    List<Good> getUserOrders(String login);
}
