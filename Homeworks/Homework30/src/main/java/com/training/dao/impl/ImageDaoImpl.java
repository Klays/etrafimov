package com.training.dao.impl;

import com.training.dao.ImageDao;
import com.training.model.Image;
import lombok.RequiredArgsConstructor;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class ImageDaoImpl implements ImageDao {

    @PersistenceContext
    private EntityManager entityManager;

    private final SessionFactory sessionFactory;

    private static final String GET_IMAGE_BY_ID_QUERY = "FROM Image i WHERE i.id = :id";

    private static final String SELECT_ALL = "FROM Image";

    @Override
    public Image save(Image image) {
        Long id = (Long) sessionFactory.getCurrentSession().save(image);
        Image newImage = new Image();
        newImage.setName(image.getName());
        newImage.setImage(image.getImage());
        newImage.setId(id);
        return newImage;
    }

    @Override
    public List<Image> findAllImages() {
        return sessionFactory.getCurrentSession().createQuery(SELECT_ALL, Image.class)
                .getResultList();
    }

    @Override
    public Image getById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Invalid user id");
        }
        return sessionFactory.getCurrentSession()
                .createQuery(GET_IMAGE_BY_ID_QUERY, Image.class).uniqueResult();
    }
}
