package com.training.dao.impl;

import com.training.dao.UserDao;
import com.training.model.User;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Slf4j
@Repository
@RequiredArgsConstructor
public class UserDaoIml implements UserDao {

    private static final String GET_USER_BY_ID_QUERY = "FROM User u WHERE u.id = :id";

    private static final String SELECT_ALL = "FROM User";

    private static final String SELECT_BY_LOGIN = "FROM User u WHERE u.login = :login";

    private final SessionFactory sessionFactory;

    @Override
    public User save(User user) {
        Long id = (Long) sessionFactory.getCurrentSession().save(user);
        User newUser = new User();
        newUser.setLogin(user.getLogin());
        newUser.setPassword(user.getPassword());
        newUser.setId(id);
        return newUser;
    }

    @Override
    public List<User> getAll() {
        return sessionFactory.getCurrentSession().createQuery(SELECT_ALL, User.class)
                .getResultList();
    }

    @Override
    public User getById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Invalid user id");
        }
        return sessionFactory.getCurrentSession()
                .createQuery(GET_USER_BY_ID_QUERY, User.class).uniqueResult();
    }

    @SneakyThrows
    @Override
    public User getByLogin(String login) {
        if (login == null) {
            throw new Exception("Invalid login");
        }
        return sessionFactory.getCurrentSession().createQuery(SELECT_BY_LOGIN, User.class)
                .setParameter("login", login)
                .uniqueResult();
    }

}
