package com.training.dao;

import com.training.model.Good;

import java.util.List;

public interface GoodsDao {

    List<Good> getAll();

    Good getById(Long id);

    Good save(Good good);

    Good update(Long id, Good good);

}
