package com.training.dao.impl;

import com.training.Dto.OrderDto;
import com.training.dao.OrdersDao;
import com.training.model.Order;
import lombok.RequiredArgsConstructor;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.validation.Valid;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class OrdersDaoIml implements OrdersDao {

    private static final String SELECT_ALL = "FROM Order";

    private static final String GET_GOOD_QUERY = "FROM Order o WHERE o.id = :id";

    private final SessionFactory sessionFactory;

    @Override
    public Order saveOrder(Order order) {
        Long id = (Long) sessionFactory.getCurrentSession().save(order);
        Order newOrder = new Order();
        newOrder.setTotalPrice(order.getTotalPrice());
        newOrder.setGoods(order.getGoods());
        newOrder.setId(id);
        newOrder.setUser(order.getUser());
        return newOrder;
    }

    @Override
    public Order update(Long id, Order order) {
        Order newOrder = findById(id);
        newOrder.setGoods(order.getGoods());
        newOrder.setTotalPrice(order.getTotalPrice());
        return newOrder;
    }

    @Override
    public List<Order> findAll() {
        return sessionFactory.getCurrentSession()
                .createQuery(SELECT_ALL, Order.class).getResultList();
    }

    @Override
    public Order findById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Invalid id");
        }
        return sessionFactory.getCurrentSession()
                .createQuery(GET_GOOD_QUERY, Order.class)
                .setParameter("id", id)
                .uniqueResult();
    }

    @Override
    public void delete(Long id) {
        sessionFactory.getCurrentSession().delete(id);
    }

    @Override
    public String sendEmail(@Valid OrderDto orderDto) {
        return null;
    }
}
