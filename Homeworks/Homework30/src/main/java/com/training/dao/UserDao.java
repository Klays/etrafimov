package com.training.dao;

import com.training.model.User;

import java.util.List;

public interface UserDao {

    User save(User user);

    List<User> getAll();

    User getById(Long id);

    User getByLogin(String login);

}
