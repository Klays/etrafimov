package com.training.exceptions;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

public class ExceptionsHandler {

    @ExceptionHandler(Exception.class)
    public ModelAndView handlerException(Exception exception) {

        ModelAndView modelAndView = new ModelAndView();

        modelAndView.addObject("error", exception.toString());
        modelAndView.addObject("errors", exception.getMessage());

        modelAndView.setViewName("error");

        return modelAndView;
    }

}
