package com.training.service.impl;

import com.training.dao.ImageDao;
import com.training.model.Image;
import com.training.service.ImageService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ImageServiceImpl implements ImageService {

    private final ImageDao imageDao;

    @Override
    @Transactional
    public void save(Image image) {
        imageDao.save(image);
    }

    @Override
    @Transactional
    public List<Image> findAllImages() {
        return imageDao.findAllImages();
    }

    @Override
    @Transactional
    public Image findById(Long id) {
        return imageDao.getById(id);
    }
}
