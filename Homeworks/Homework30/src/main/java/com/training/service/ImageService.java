package com.training.service;

import com.training.model.Image;

import java.util.List;

public interface ImageService {

    void save(Image image);

    List<Image> findAllImages();

    Image findById(Long id);

}
