package com.training.Dto;

import com.training.model.Good;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
public class OrderDto {

    private BigDecimal totalPrice;

    private List<Good> goods = new ArrayList<>();
}
