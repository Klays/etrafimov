package by.training.util;

import by.training.model.User;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public final class UserMailExtractor {


    private UserMailExtractor() {
    }

    public static List<String> getMailsFromUsers(List<User> users) {
        log.info("Received emails from users");
        return users.stream()
                .map(User::getEmail)
                .collect(Collectors.toList());
    }
}
