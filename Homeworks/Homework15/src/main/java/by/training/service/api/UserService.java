package by.training.service.api;

import by.training.model.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    Optional<User> getCurrentUser();

    List<User> getDevelopers();
}
