package by.training.service.api;

import by.training.model.User;

import java.util.List;

public interface MessageCreator {

    String createMessage(List<String> recipients, String theme);

    String createPersonalMessage(User user, String theme);
}
