package by.training.service.api;

import by.training.model.User;

import java.util.Map;

public interface MailService {

    void sendMessageAboutBug();

    String sendFirstInvitation(User user);

    void sendMeDummyMessagesForAllTopics();

    Map<String, User> getDeveloperEmails();
}
