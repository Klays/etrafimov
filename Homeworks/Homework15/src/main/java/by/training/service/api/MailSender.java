package by.training.service.api;

public interface MailSender {

    void sendMail(String mail);
}
