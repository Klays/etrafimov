package by.training.service.impl;

import by.training.exception.RecipientsException;
import by.training.model.User;
import by.training.model.enums.Topic;
import by.training.service.api.MailSender;
import by.training.service.api.MailService;
import by.training.service.api.MessageCreator;
import by.training.service.api.UserService;
import by.training.util.UserMailExtractor;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static by.training.model.enums.Topic.BUG;
import static by.training.model.enums.Topic.TASK;

@Slf4j
public class MailServiceImpl implements MailService {

    private final MessageCreator messageCreator;
    private final UserService userService;
    private final MailSender mailSender;

    public MailServiceImpl(MessageCreator messageCreator, UserService userService, MailSender mailSender) {
        this.messageCreator = messageCreator;
        this.userService = userService;
        this.mailSender = mailSender;
    }

    @Override
    public void sendMessageAboutBug() {
        List<User> developers = userService.getDevelopers();

        List<String> recipients = UserMailExtractor.getMailsFromUsers(developers);

        checkRecipients(recipients);

        String message = messageCreator.createMessage(recipients, BUG.getText());
        log.info("Sending mail: {}", message);
        mailSender.sendMail(message);
    }

    @Override
    public String sendFirstInvitation(User user) {
        String personalMessage = messageCreator.createPersonalMessage(user, TASK.getText());

        mailSender.sendMail(personalMessage);
        log.info("Personal message: {}", personalMessage);
        return personalMessage;
    }

    @Override
    public void sendMeDummyMessagesForAllTopics() {
        User currentUser = userService.getCurrentUser()
                .orElseThrow(NoSuchElementException::new);

        Set<String> messages = getMessagesForUser(currentUser);

        log.info("Messages for all topics: {}", messages);

        messages.forEach(mailSender::sendMail);
    }

    @Override
    public Map<String, User> getDeveloperEmails() {
        log.info("Developer messages");
        return userService.getDevelopers().stream()
                .collect(Collectors.toMap(User::getEmail, Function.identity()));
    }

    private void checkRecipients(List<String> recipients) {
        if (recipients.isEmpty()) {
            throw new RecipientsException("Recipients list is empty.");
        }
    }

    private Set<String> getMessagesForUser(User currentUser) {
        log.info("User messages");
        return Arrays.stream(Topic.values())
                .map(Topic::getText)
                .map(text -> messageCreator.createPersonalMessage(currentUser, text))
                .collect(Collectors.toSet());
    }
}
