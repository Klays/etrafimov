package by.training.exception;

public class RecipientsException extends RuntimeException {

    public RecipientsException(String message) {
        super(message);
    }
}
