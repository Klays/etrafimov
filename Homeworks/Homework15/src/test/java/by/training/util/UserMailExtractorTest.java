package by.training.util;

import by.training.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class UserMailExtractorTest {

    public static final String EMAIL = "1";

    @Test
    void testGetMailsFromUsers() {
        List<String> result = UserMailExtractor.getMailsFromUsers(createDevelopers());
        Assertions.assertTrue( result.contains(EMAIL));

    }

    private User createUser(String firstName, String lastName, String email) {
        User user = new User();
        user.setEmail(email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        return user;
    }

    private List<User> createDevelopers() {
        List<User> dev = new ArrayList<>();
        dev.add(createUser("1", "1", EMAIL));
        return dev;
    }
}