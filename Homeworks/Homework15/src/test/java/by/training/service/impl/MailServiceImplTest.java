package by.training.service.impl;

import by.training.exception.RecipientsException;
import by.training.model.User;
import by.training.service.api.MailSender;
import by.training.service.api.MessageCreator;
import by.training.service.api.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class MailServiceImplTest {

    @Mock
    private MessageCreator messageCreator;

    @Mock
    private UserService userService;

    @Mock
    private MailSender mailSender;

    @InjectMocks
    private MailServiceImpl mailService;

    @Test
    void testSendMessageAboutBug() {
        String message = "test";
        List<User> users = createDevelopers();
        Mockito.when(userService.getDevelopers()).thenReturn(users);
        Mockito.when(messageCreator.createMessage(Mockito.anyList(), Mockito.any())).thenReturn(message);
        mailService.sendMessageAboutBug();
        Mockito.verify(mailSender, Mockito.times(1)).sendMail(message);
    }

    @Test
    void testSendFirstInvitation() {
        String message = "ggg";
        Mockito.when(messageCreator.createPersonalMessage(Mockito.any(), Mockito.any())).thenReturn(message);
        String result = mailService.sendFirstInvitation(new User());
        Assertions.assertEquals(message, result);
        Mockito.verify(mailSender, Mockito.times(1)).sendMail(message);
    }

    @Test
    void testSendMeDummyMessagesForAllTopics() {
        String text = "1";
        String text1 = "2";
        String text2 = "3";
        Mockito.when(userService.getCurrentUser()).thenReturn(Optional.of(new User()));
        Mockito.when(messageCreator.createPersonalMessage(Mockito.any(), Mockito.any())).thenReturn(text)
                .thenReturn(text1).thenReturn(text2);
        mailService.sendMeDummyMessagesForAllTopics();
        Mockito.verify(mailSender, Mockito.times(1)).sendMail(text);
        Mockito.verify(mailSender, Mockito.times(1)).sendMail(text1);
        Mockito.verify(mailSender, Mockito.times(1)).sendMail(text2);
        Mockito.verify(mailSender, Mockito.times(3)).sendMail(Mockito.any());
    }

    @Test
    void testCheckRecipients() {

        List<User> users = createDevelopers();
        Mockito.when(userService.getDevelopers()).thenReturn(users);
        Map<String, User> result = mailService.getDeveloperEmails();
        Assertions.assertTrue(result.containsKey(users.get(0).getEmail()));
    }

    @Test
    void testSendMessageAboutBugWithException() {
        Mockito.when(userService.getDevelopers()).thenReturn(Collections.emptyList());
        RecipientsException exception = assertThrows(
                RecipientsException.class,
                () -> mailService.sendMessageAboutBug()
        );
        assertTrue(exception.getMessage().contains("Recipients list is empty."));
    }

    private User createUser(String firstName, String lastName, String email) {
        User user = new User();
        user.setEmail(email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        return user;
    }

    private List<User> createDevelopers() {
        List<User> dev = new ArrayList<>();
        dev.add(createUser("1", "1", "1"));
        return dev;
    }

}