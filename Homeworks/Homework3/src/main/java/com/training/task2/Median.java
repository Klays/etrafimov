package com.training.task2;

public final class Median {
    /**
     * Median constructor prevents parameterless constructor.
     */
    private Median() {
    }

    /**
     * Method for calculating the median of integers and doubles.
     * @param array The array where you want to find the median.
     * @return Returns the median.
     */
    public static double median(double[] array) {
        if (array.length % 2 == 0) {
            int mid = array.length / 2;
            return (array[mid] + array[mid - 1]) / 2;
        }
        return array[array.length / 2];
    }

    public static int median(int[] array) {
        if (array.length % 2 == 0) {
            int mid = array.length / 2;
            return (array[mid] + array[mid - 1]) / 2;
        }
        return array[array.length / 2];
    }
}

