package com.training.task1;

import java.math.BigDecimal;

public class Card {

    private final String ownerName;
    private BigDecimal balance = BigDecimal.valueOf(0);

    public Card(String ownerName) {
        this.ownerName = ownerName;
    }

    public Card(String ownerName, BigDecimal balance) {
        this.ownerName = ownerName;
        this.balance = balance;
    }

    /**
     * Funding method.
     * @param cash Replenishment variable.
     * @return Returns the balance after replenishment of the account.
     */
    public BigDecimal topUpBalance(BigDecimal cash) {
        if (cash.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException("Negative number");
        } else if (balance == null) {
            throw new NullPointerException("Null");
        } else {
            this.balance = this.balance.add(cash);
            return balance;
        }
    }

    /**
     * Method of withdrawing money from the card.
     * @param withdrawCash Withdraw variable.
     * @return Returns the balance after withdrawing money from the account.
     * @throws IllegalArgumentException Doesn't allow you to withdraw more
     * from your account than you have.
     */
    public BigDecimal withdraw(BigDecimal withdrawCash) {
        if (withdrawCash.compareTo(this.balance) > 0) {
            throw new IllegalArgumentException("insufficient funds");
        } else if(balance==null){
            throw new NullPointerException("Null");
        }else if (withdrawCash.compareTo(BigDecimal.ZERO) < 0){
            throw new IllegalArgumentException("Negative number");
        }
        else {
            this.balance = this.balance.subtract(withdrawCash);
            return balance;
        }
    }

    /**
     * A method that allows you to find out the balance in the required currency.
     * @param currency Currency parameter (BYN, USD, RUB, EUR).
     * @return Returns the balance in the required currency.
     */
    public BigDecimal convert(DifferentCurrency currency) {
        return balance = this.balance.multiply(BigDecimal.valueOf(currency.getExchangeRate()));
    }
}
