package com.training.task1;

/**
 * Enum сontaining a currency with a rate.
 */
public enum DifferentCurrency {
    USD(2.5D),
    EUR(2.9D),
    BYN(1.0D),
    RYB(29.5D);

    double exchangeRate;

    public double getExchangeRate() {
        return this.exchangeRate;
    }

    private DifferentCurrency(double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }
}
