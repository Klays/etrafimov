package com.training.task1;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class CardTest {

    @Test
    void testWithdrawFromTheCard() {
        Card card = new Card("Evgeny", BigDecimal.valueOf(200));
        BigDecimal balance = card.withdraw(BigDecimal.valueOf(100));
        assertEquals(BigDecimal.valueOf(100), balance);
    }

    @Test
    void testWithdrawingMoreMoneyFromTheCard() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    Card card = new Card("Evgeny", BigDecimal.valueOf(100));
                    BigDecimal balance = card.withdraw(BigDecimal.valueOf(200));
                });
    }

    @Test
    void testWithdrawingNegative() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    Card card = new Card("Evgeny", BigDecimal.valueOf(200));
                    BigDecimal balance = card.withdraw(BigDecimal.valueOf(-100));
                });
    }

    @Test
    void testWithdrawingNull() {
        assertThrows(NullPointerException.class,
                () -> {
                    Card card = new Card("Evgeny", BigDecimal.valueOf(200));
                    BigDecimal balance = card.withdraw(null);
                });
    }

    @Test
    void testTopUpBalance() {
        Card card = new Card("Evgeny", BigDecimal.valueOf(100));
        BigDecimal balance = card.topUpBalance(BigDecimal.valueOf(100));
        assertEquals(BigDecimal.valueOf(200), balance);
    }

    @Test
    void testTopUpBalanceNegative() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    Card card = new Card("Evgeny", BigDecimal.valueOf(200));
                    BigDecimal balance = card.topUpBalance(BigDecimal.valueOf(-100));
                });
    }

    @Test
    void testTopUpBalanceNull() {
        assertThrows(NullPointerException.class,
                () -> {
                    Card card = new Card("Evgeny", BigDecimal.valueOf(200));
                    BigDecimal balance = card.topUpBalance(null);
                });
    }

    @Test
    void tesConvertRYB() {
        Card card = new Card("Evgeny", BigDecimal.valueOf(100));
        BigDecimal balanceInCurrency = card.convert(DifferentCurrency.BYN);
        assertEquals(BigDecimal.valueOf(100.0), balanceInCurrency);
    }
}