package com.training.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.training.config.WebConfig;
import com.training.dto.AuthRequest;
import com.training.model.User;
import com.training.service.UserService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@ContextConfiguration(classes = WebConfig.class)
@WebAppConfiguration
@ExtendWith(SpringExtension.class)
class AuthenticationControllerTest {

    public static final String LOGIN = "test@gggmail.com";
    public static final String PASSWORD = "testTest123";

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    AuthenticationController authenticationController;

    @Autowired
    private UserService userService;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
        this.mockMvc = builder.build();
    }

    @SneakyThrows
    @Test
    void createAuthenticationToken() {
        AuthRequest authRequest = new AuthRequest();
        authRequest.setLogin(LOGIN);
        authRequest.setPassword(PASSWORD);

        authenticationController.registerUser(authRequest);

        ObjectMapper objectMapper = new ObjectMapper();
        String test = objectMapper.writeValueAsString(authRequest);

        mockMvc.perform(MockMvcRequestBuilders.post("/authenticate").contentType(MediaType.APPLICATION_JSON).content(test))
                .andExpect(MockMvcResultMatchers.status().isOk());

        User user = userService.findByLogin(LOGIN);

        Assertions.assertNotNull(user);
    }

    @SneakyThrows
    @Test
    void registerUser() {

        AuthRequest authRequest = new AuthRequest();
        authRequest.setLogin(LOGIN);
        authRequest.setPassword(PASSWORD);

        ObjectMapper objectMapper = new ObjectMapper();
        String test = objectMapper.writeValueAsString(authRequest);

        mockMvc.perform(MockMvcRequestBuilders.post("/register").contentType(MediaType.APPLICATION_JSON).content(test))
                .andExpect(MockMvcResultMatchers.status().isOk());

        User user = userService.findByLogin(LOGIN);

        Assertions.assertNotNull(user);
    }
}