package com.training.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.training.config.WebConfig;
import com.training.model.User;
import com.training.service.UserService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@ContextConfiguration(classes = WebConfig.class)
@WebAppConfiguration
@ExtendWith(SpringExtension.class)
class UserControllerTest {

    public static final String LOGIN = "test@gg.com";
    public static final String PASSWORD = "testPassword";
    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private UserService userService;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac)
                .build();
    }

    @SneakyThrows
    @Test
    void testSave() {

        User user = new User();
        user.setLogin(LOGIN);
        user.setPassword(PASSWORD);

        ObjectMapper objectMapper = new ObjectMapper();
        String test = objectMapper.writeValueAsString(user);

        mockMvc.perform(MockMvcRequestBuilders.post("/users")
                .contentType(MediaType.APPLICATION_JSON).content(test))
                .andExpect(MockMvcResultMatchers.status().isCreated());

        User newUser = userService.findByLogin(LOGIN);
        Assertions.assertNotNull(newUser);
    }

    @SneakyThrows
    @Test
    void testFindAll() {
        User user = new User();
        user.setLogin(LOGIN);
        user.setPassword(PASSWORD);
        userService.save(user);

        mockMvc.perform(MockMvcRequestBuilders.get("/users"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$[0].login").value(LOGIN))
                .andExpect(jsonPath("$[0].password").value(PASSWORD));
    }

    @SneakyThrows
    @Test
    void testFindById() {

        User user = new User();
        user.setId(5L);
        user.setLogin(LOGIN);
        user.setPassword(PASSWORD);
        userService.save(user);

        mockMvc.perform(MockMvcRequestBuilders.get("/users/{id}", 5))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.login").value(LOGIN))
                .andExpect(jsonPath("$.password").value(PASSWORD));
    }
}