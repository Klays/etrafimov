package com.training.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.training.config.WebConfig;
import com.training.dao.GoodsDao;
import com.training.model.Good;
import com.training.service.GoodService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@ContextConfiguration(classes = WebConfig.class)
@WebAppConfiguration
@ExtendWith(SpringExtension.class)
class GoodControllerTest {

    public static final String VALUE = "Pen";
    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private GoodService goodService;

    @Autowired
    private GoodsDao goodsDao;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
        this.mockMvc = builder.build();
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    @SneakyThrows
    @Test
    void testSave() {
        Good good = new Good();
        good.setName("testName");
        good.setPrice(BigDecimal.valueOf(100));
        goodsDao.save(good);

        ObjectMapper objectMapper = new ObjectMapper();
        String test = objectMapper.writeValueAsString(good);

        mockMvc.perform(MockMvcRequestBuilders.post("/goods")
                .contentType(MediaType.APPLICATION_JSON).content(test))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @SneakyThrows
    @Test
    void testFindById() {
        mockMvc.perform(MockMvcRequestBuilders.get("/goods/{id}", 1))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.name").value(VALUE))
                .andExpect(jsonPath("$.price").value(BigDecimal.valueOf(1.0)));
    }

    @SneakyThrows
    @Test
    void testFindAll() {

        mockMvc.perform(MockMvcRequestBuilders.get("/goods"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$[0].name").value(VALUE))
                .andExpect(jsonPath("$[0].price").value(BigDecimal.valueOf(1.0)));
    }
}