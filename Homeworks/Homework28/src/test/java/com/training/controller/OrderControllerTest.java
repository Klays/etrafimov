package com.training.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.training.config.WebConfig;
import com.training.config.security.WebSecurityConfig;
import com.training.dao.GoodsDao;
import com.training.dto.AuthRequest;
import com.training.dto.AuthResponse;
import com.training.dto.OrderDto;
import com.training.model.Good;
import com.training.model.Order;
import com.training.service.OrderService;
import com.training.service.UserService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@ActiveProfiles("test")
@ContextConfiguration(classes = {WebConfig.class, WebSecurityConfig.class})
@WebAppConfiguration
@ExtendWith(SpringExtension.class)
class OrderControllerTest {

    public static final String LOGIN = "Test@gg.com";
    public static final String PASSWORD = "Test123";
    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private OrderService orderService;

    @Autowired
    private GoodsDao goodsDao;

    @Autowired
    private UserService userService;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac)
                .build();
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    @SneakyThrows
    @Test
    void sendEmail() {
        Good good = goodsDao.getById(1L);

        List<Good> goods = new ArrayList<>();
        goods.add(good);

        AuthRequest authRequest = new AuthRequest();
        authRequest.setLogin(LOGIN);
        authRequest.setPassword(PASSWORD);

        userService.register(authRequest);

        AuthResponse authResponse = userService.authentication(authRequest);
        String token = authResponse.getJwtToken();

        OrderDto orderDto = new OrderDto();
        orderDto.setGoods(goods);
        orderDto.setTotalPrice(BigDecimal.valueOf(100));

        ObjectMapper objectMapper = new ObjectMapper();
        String test = objectMapper.writeValueAsString(orderDto);

        List<GrantedAuthority> authorities = AuthorityUtils.commaSeparatedStringToAuthorityList("Admin");
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthenticationToken(
                        "gg@gg.com", null, authorities);
        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);

        mockMvc.perform(MockMvcRequestBuilders.post("/orders")
                .header("Authorization", "Bearer " + token)
                .contentType(MediaType.APPLICATION_JSON).content(test))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    @SneakyThrows
    @Test
    void findById() {
        Good good = goodsDao.getById(1L);

        List<Good> goods = new ArrayList<>();
        goods.add(good);

        Order order = new Order();
        order.setGoods(goods);
        order.setTotalPrice(BigDecimal.valueOf(100));
        orderService.save(order);

        mockMvc.perform(MockMvcRequestBuilders.get("/orders/{id}", 1))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.goods[0].name").value("Pen"))
                .andExpect(jsonPath("$.totalPrice").value(BigDecimal.valueOf(100)));
    }
}