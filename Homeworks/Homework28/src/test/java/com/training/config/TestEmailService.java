package com.training.config;

import com.training.service.EmailService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Map;

@Service
@Profile("test")
public class TestEmailService implements EmailService {

    @Override
    public void sendEmail(String to, String subject, Map<String, Object> templateModel) throws IOException, MessagingException {
        System.out.println("Sending mail plus to " + to);
    }
}
