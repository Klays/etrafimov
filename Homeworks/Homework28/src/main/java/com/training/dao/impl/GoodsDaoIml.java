package com.training.dao.impl;

import com.training.dao.GoodsDao;
import com.training.model.Good;
import com.training.model.User;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Slf4j
@Repository
@RequiredArgsConstructor
public class GoodsDaoIml implements GoodsDao {

    private static final String GET_GOOD_QUERY = "FROM Good g WHERE g.id = :id";

    private static final String SELECT_BY_ORDER = "FROM Good g WHERE g.name = :name";

    private static final String SELECT_ALL = "FROM Good";

    private final SessionFactory sessionFactory;

    @Override
    public List<Good> getAll() {
        return sessionFactory.getCurrentSession()
                .createQuery(SELECT_ALL, Good.class).getResultList();
    }

    @Override
    public Good getById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Invalid id");
        }
        return sessionFactory.getCurrentSession()
                .createQuery(GET_GOOD_QUERY, Good.class)
                .setParameter("id", id)
                .uniqueResult();

    }

    @Override
    public Good save(Good good) {
        Long id = (Long) sessionFactory.getCurrentSession().save(good);
        Good newGood = new Good();
        newGood.setName(good.getName());
        newGood.setPrice(good.getPrice());
        newGood.setId(id);
        return newGood;
    }

    @Override
    public Good update(Long id, Good good) {
        Good newGood = getById(id);
        newGood.setName(good.getName());
        newGood.setPrice(good.getPrice());
        return newGood;
    }

    @SneakyThrows
    @Override
    public Good getByLogin(String name) {
        if (name == null) {
            throw new Exception("Invalid name");
        }
        return sessionFactory.getCurrentSession().createQuery(SELECT_BY_ORDER, Good.class)
                .setParameter("name", name)
                .uniqueResult();
    }
}
