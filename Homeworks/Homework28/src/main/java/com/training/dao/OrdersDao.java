package com.training.dao;

import com.training.dto.OrderDto;
import com.training.model.Order;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;

public interface OrdersDao {

    Order saveOrder(Order order);

    Order update(Long id, Order order);

    List<Order> findAll();

    Order findById(Long id);

    void delete(Long id);

    String sendEmail(@Valid @RequestBody OrderDto orderDto);
}
