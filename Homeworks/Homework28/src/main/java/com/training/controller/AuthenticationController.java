package com.training.controller;

import com.training.dto.AuthRequest;
import com.training.dto.AuthResponse;
import com.training.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class AuthenticationController {

    private final UserService userService;

    @PostMapping("/authenticate")
    @ResponseStatus(HttpStatus.OK)
    public AuthResponse createAuthenticationToken(@RequestBody AuthRequest authRequest) {
        return userService.authentication(authRequest);
    }

    @PostMapping("/register")
    public String registerUser(@RequestBody AuthRequest authRequest) {
        return userService.register(authRequest);
    }

}
