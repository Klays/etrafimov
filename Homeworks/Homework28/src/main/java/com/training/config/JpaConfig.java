package com.training.config;//package com.training.config;
//
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.PropertySource;
//import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
//import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
//import org.springframework.jdbc.datasource.DriverManagerDataSource;
//import org.springframework.orm.jpa.JpaTransactionManager;
//import org.springframework.orm.jpa.JpaVendorAdapter;
//import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
//import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
//import org.springframework.transaction.PlatformTransactionManager;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//
//import javax.sql.DataSource;
//import java.util.Properties;
//
//import static org.hibernate.cfg.AvailableSettings.*;
//
//@Configuration
//@EnableTransactionManagement
//@ComponentScan(basePackages = {"com.training"})
//@PropertySource("classpath:db.properties")
//public class JpaConfig {
//
//    public static final String ENTITY_PATH = "by.training.model";
//
//    @Value("${datasource.driverClassName}")
//    private String driverClassName;
//
//    @Value("${datasource.url}")
//    private String jdbcUrl;
//
//    @Value("${datasource.username}")
//    private String jdbcUsername;
//
//    @Value("${datasource.password}")
//    private String getJdbcPassword;
//
//    @Value("${hibernate.dialect}")
//    private String hibernateDialect;
//
//    @Value("${hibernate.hbm2ddl.auto}")
//    private String hibernateHbm2DdlAuto;
//
//    @Value("${hibernate.show_sql}")
//    private String hibernateShowSql;
//
//    @Value("${hibernate.format_sql}")
//    private String hibernateFormatSql;
//
//    @Bean
//    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
//        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
//        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
//        factoryBean.setDataSource(dataSource());
//        factoryBean.setPackagesToScan(ENTITY_PATH);
//        factoryBean.setJpaVendorAdapter(vendorAdapter);
//        factoryBean.setJpaProperties(additionalProperties());
//        return factoryBean;
//    }
//
//    @Bean
//    public DataSource dataSource() {
//        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
//        dataSource.setDriverClassName(driverClassName);
//        dataSource.setUrl(jdbcUrl);
//        dataSource.setUsername(jdbcUsername);
//        dataSource.setPassword(getJdbcPassword);
//
//        return dataSource;
//    }
//
//    @Bean
//    public PlatformTransactionManager transactionManager() {
//        final JpaTransactionManager transactionManager = new JpaTransactionManager();
//        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
//        return transactionManager;
//    }
//
//    @Bean
//    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
//        return new PersistenceExceptionTranslationPostProcessor();
//    }
//
//    private Properties additionalProperties() {
//        final Properties hibernateProperties = new Properties();
//        hibernateProperties.setProperty(HBM2DDL_AUTO, hibernateHbm2DdlAuto);
//        hibernateProperties.setProperty(DIALECT, hibernateDialect);
//        hibernateProperties.put(SHOW_SQL, hibernateShowSql);
//        hibernateProperties.put(FORMAT_SQL, hibernateFormatSql);
//        return hibernateProperties;
//    }
//
//    @Bean
//    public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
//        return new PropertySourcesPlaceholderConfigurer();
//    }
//}
