package com.training.service.impl;

import com.training.dto.OrderDto;
import com.training.dao.OrdersDao;
import com.training.model.Good;
import com.training.model.Order;
import com.training.model.User;
import com.training.service.EmailService;
import com.training.service.OrderService;
import com.training.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrdersDao ordersDao;

    private final UserService userService;

    private final EmailService emailService;

    @Override
    @Transactional
    public Order update(Long id, Order order) {
        return ordersDao.update(id, order);
    }

    @Override
    @SneakyThrows
    @Transactional
    public Order create(OrderDto orderDto) {

        String userPrincipal = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userService.findByLogin(userPrincipal);
        List<Good> goods = orderDto.getGoods();
        BigDecimal totalPrice = orderDto.getTotalPrice();

        Order order = new Order();
        order.setUser(user);
        order.setGoods(goods);
        order.setTotalPrice(totalPrice);

        Order newOrder = ordersDao.saveOrder(order);

        Map<String, Object> allOrder = new HashMap<>();

        allOrder.put("order", order.getGoods());
        allOrder.put("totalPrice", order.getTotalPrice());

        emailService.sendEmail(order.getUser().getLogin(), "order", allOrder);
        return newOrder;
    }

    @Override
    @Transactional
    public Order save(Order order) {
        return ordersDao.saveOrder(order);
    }

    @Override
    @Transactional
    public List<Order> findAll() {
        return ordersDao.findAll();
    }

    @Override
    @Transactional
    public Order findById(Long id) {
        return ordersDao.findById(id);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        ordersDao.delete(id);
    }
}
