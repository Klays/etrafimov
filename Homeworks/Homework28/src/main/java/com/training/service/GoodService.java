package com.training.service;

import com.training.model.Good;

import java.math.BigDecimal;
import java.util.List;

public interface GoodService {

    Good update(Long id, Good good);

    Good save(Good good);

    List<Good> findAll();

    Good findById(Long id);

    BigDecimal getTotalPrice(List<Good> goods);

    Good findByLogin(String name);
}
