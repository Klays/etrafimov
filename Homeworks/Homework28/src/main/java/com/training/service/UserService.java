package com.training.service;

import com.training.dto.AuthRequest;
import com.training.dto.AuthResponse;
import com.training.model.User;

import java.util.List;

public interface UserService {

    List<User> findAll();

    User findById(Long id);

    User save(User user);

    User findByLogin(String login);

    String register(AuthRequest authRequest);

    AuthResponse authentication(AuthRequest authRequest);
}
