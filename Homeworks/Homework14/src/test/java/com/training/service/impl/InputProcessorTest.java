package com.training.service.impl;

import com.training.enums.HttpMethod;
import com.training.dto.Post;
import com.training.service.InputProcessor;
import com.training.service.WebClient;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Map;

@ExtendWith(MockitoExtension.class)
class InputProcessorTest {

    public static final long ID = 6L;
    public static final String USER_ID = "UserId";
    public static final String BODY = "Body";

    private static InputProcessor inputProcessor;
    private static final ApacheWebClient apacheWebClient = Mockito.mock(ApacheWebClient.class);
    private static final HttpURLConnectionWebClient httpURLConnectionWebClient = Mockito.mock(HttpURLConnectionWebClient.class);

    @BeforeAll
    static void before() {
        Map<String, WebClient> clientMap = new HashMap<>();
        clientMap.put("task1", httpURLConnectionWebClient);
        clientMap.put("task2", apacheWebClient);
        inputProcessor = new InputProcessor(clientMap);
    }

    @SneakyThrows
    @Test
    void testProcessTask1Get() {
        Post expected = createPost("Title");
        Mockito.when(httpURLConnectionWebClient.getPostById(Mockito.anyLong())).thenReturn(expected);
        Post post = inputProcessor.process("task1", HttpMethod.GET, "2");
        Assertions.assertEquals(expected.getBody(), post.getBody());
    }

    private Post createPost(String title) {

        Post createPost = new Post();
        createPost.setBody(BODY);
        createPost.setTitle(title);
        createPost.setUserId(USER_ID);
        createPost.setId(ID);

        return createPost;
    }
}