package com.training;

import com.training.enums.HttpMethod;
import com.training.dto.Post;
import com.training.service.impl.ApacheWebClient;
import com.training.service.impl.HttpURLConnectionWebClient;
import com.training.service.InputProcessor;
import com.training.service.WebClient;

import java.util.Map;
import java.util.TreeMap;

public class Main {
    public static void main(String[] args) {
        InputProcessor inputProcessor = new InputProcessor(initMap());

        Post a = inputProcessor.process(args[0], HttpMethod.valueOf(args[1]), args[2]);
        System.out.println(a.getTitle());
    }
    private static Map<String, WebClient> initMap() {

        Map<String, WebClient> map = new TreeMap<>();
        map.put("task1", new HttpURLConnectionWebClient());
        map.put("task2", new ApacheWebClient());

        return map;
    }
}
