package com.training.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.training.dto.Post;

import java.io.IOException;

public interface WebClient {

    String APPLICATION_JSON = "application/json";
    String CONTENT_TYPE = "Content-Type";
    String ACCEPT = "Accept";

    ObjectMapper mapper = new ObjectMapper();

    String GET_ENDPOINT = "https://jsonplaceholder.typicode.com/posts/";
    String POST_ENDPOINT = "https://jsonplaceholder.typicode.com/posts";

    Post getPostById(Long id) throws IOException;

    Post publishPost(Post post) throws IOException;

}
