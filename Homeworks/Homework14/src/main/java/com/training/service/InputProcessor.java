package com.training.service;

import com.training.enums.HttpMethod;
import com.training.dto.Post;
import lombok.SneakyThrows;

import java.util.Map;
import java.util.Optional;

public class InputProcessor {

    private final Map<String, WebClient> clientMap;

    public InputProcessor(Map<String, WebClient> clientMap) {
        this.clientMap = clientMap;
    }


    @SneakyThrows
    public Post process(String task, HttpMethod method, String param) {

        Post post;

        WebClient client = Optional.ofNullable(clientMap.get(task)).orElseThrow(() -> new IllegalArgumentException("No client available with key: " + task));
        if (HttpMethod.GET.equals(method)) {
            post = client.getPostById(Long.valueOf(param));
        } else if (HttpMethod.POST.equals(method)) {
            Post createPost = new Post();
            createPost.setBody("Body");
            createPost.setTitle(param);
            createPost.setUserId("UserId");
            post = client.publishPost(createPost);
        } else {
            throw new IllegalArgumentException("method is null");
        }
        return post;
    }
}
