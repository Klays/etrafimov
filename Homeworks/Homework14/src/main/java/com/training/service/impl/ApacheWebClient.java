package com.training.service.impl;

import com.training.dto.Post;
import com.training.service.WebClient;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class ApacheWebClient implements WebClient {

    private final CloseableHttpClient httpClient = HttpClients.createDefault();

    @Override
    public Post getPostById(Long id) throws IOException {
        HttpGet request = new HttpGet(GET_ENDPOINT + id);

        String result = "";

        try (CloseableHttpResponse response = httpClient.execute(request)) {

            HttpEntity entity = response.getEntity();

            if (entity != null) {
                result = EntityUtils.toString(entity);
            }
            return mapper.readValue(result, Post.class);
        }
    }

    @Override
    public Post publishPost(Post post) throws IOException {

        HttpPost httpPost = new HttpPost(POST_ENDPOINT);

        httpPost.setHeader(CONTENT_TYPE, APPLICATION_JSON);

        httpPost.setEntity(new StringEntity(mapper.writeValueAsString(post)));

        CloseableHttpResponse response = httpClient.execute(httpPost);

        String result = EntityUtils.toString(response.getEntity());

        return mapper.readValue(result, Post.class);
    }

}

