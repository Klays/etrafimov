package com.training.service.impl;

import com.training.dto.Post;
import com.training.enums.HttpMethod;
import com.training.service.WebClient;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpURLConnectionWebClient implements WebClient {

    @Override
    public Post getPostById(Long id) throws IOException {

        URL url = new URL(GET_ENDPOINT + id);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setRequestProperty(CONTENT_TYPE, APPLICATION_JSON);
        connection.setRequestMethod(HttpMethod.GET.toString());

        InputStream inputStream = connection.getInputStream();

        return mapper.readValue(inputStream, Post.class);

    }

    @Override
    public Post publishPost(Post post) throws IOException {

        URL url = new URL(POST_ENDPOINT);

        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        connection.setRequestMethod(HttpMethod.POST.toString());
        connection.setDoOutput(true);
        connection.setRequestProperty(CONTENT_TYPE, APPLICATION_JSON);
        connection.setRequestProperty(ACCEPT, APPLICATION_JSON);

        InputStream inputStream = connection.getInputStream();

        try (OutputStream os = connection.getOutputStream()) {
            byte[] bytes = mapper.writeValueAsBytes(post);
            os.write(bytes, 0, bytes.length);
        }

        return mapper.readValue(inputStream, Post.class);
    }
}