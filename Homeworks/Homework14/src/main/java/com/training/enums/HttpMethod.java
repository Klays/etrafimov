package com.training.enums;

public enum HttpMethod {

    GET,
    POST
}
