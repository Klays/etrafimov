package com.training;

public class File extends FileSystem {

    private String extension;

    public File(String name, String extension, int tabCounter) {
        super(name, tabCounter);
        this.extension = extension;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    @Override
    public String toString() {
        return OperationSystem.tab(tabCounter) + name + "." + extension;
    }
}
