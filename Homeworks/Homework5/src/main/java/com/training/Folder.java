package com.training;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Folder extends FileSystem {

    private final Map<String, Folder> folders = new HashMap<>();

    private final List<File> files = new ArrayList<>();

    public Folder(String name, int tabCounter) {
        super(name, tabCounter);
    }

    public Map<String, Folder> getFolders() {
        return folders;
    }

    public List<File> getFiles() {
        return files;
    }

    public Folder getFolderByName(String name) {
        return folders.get(name);
    }

    public void addFolder(Folder folder) {
        if (!folders.containsKey(folder.name)) {
            folders.put(folder.name, folder);
        }
    }

    public void addFile(File file) {
        if (files.stream().noneMatch(el -> el.getName().equals(file.getName()) && el.getExtension().equals(file.getExtension()))) {
            files.add(file);
        }
    }

    @Override
    public String toString() {
        return OperationSystem.tab(tabCounter) + name + "/";
    }
}
