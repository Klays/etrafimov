package com.training;

import java.util.List;

import static com.training.OperationSystem.rootFolder;

public class PathProcessor {

    private void validatePath(List<String> paths) {
        for (int i = 0; i < paths.size() - 1; i++) {
            if (paths.get(i).contains(".") && !(paths.get(i + 1).isEmpty())) {
                throw new IllegalArgumentException("file cannot be added to file or folder");
            }
        }
    }

    public void process(String path) {
        if (path == null) {
            throw new IllegalArgumentException("Path is null");
        }
        List<String> paths = List.of(path.split("/"));
        validatePath(paths);
        for (int i = 1; i < paths.size(); i++) {
            String fileSystemName = paths.get(i);
            if (fileSystemName.contains(".")) {
                int index = fileSystemName.lastIndexOf(".");
                File file = new File(fileSystemName.substring(0, index), fileSystemName.substring(index + 1), i);
                Folder folder = findFolderByIndex(i, paths);
                folder.addFile(file);
            } else {
                Folder folder = new Folder(fileSystemName, i);
                Folder foundFolder = findFolderByIndex(i, paths);
                foundFolder.addFolder(folder);
            }
        }
    }

    private Folder findFolderByIndex(int index, List<String> paths) {
        return getFolderRecursively(rootFolder, 1, index, paths);
    }

    private Folder getFolderRecursively(Folder parent, int currentIndex, int lastIndex, List<String> paths) {
        if (currentIndex < lastIndex) {
            Folder folder = parent.getFolderByName(paths.get(currentIndex));
            currentIndex++;
            if (currentIndex == lastIndex) {
                return folder;
            }
            parent = getFolderRecursively(folder, currentIndex, lastIndex, paths);
        }
        return parent;
    }
}
