package com.training;

import java.util.Scanner;

import static com.training.OperationSystem.rootFolder;

public class Input {
    public void enterFromTheKeyboard() {
        PathProcessor processor = new PathProcessor();
        Scanner scanner = new Scanner(System.in);
        while (true) {
            String path = scanner.next();
            if (path.equals("STOP")) {
                System.exit(0);
            } else if (path.equals("Print")) {
                OperationSystem.print(rootFolder);
                continue;
            } else {
                processor.process(path);
            }
        }
    }
}
