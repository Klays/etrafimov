package com.training;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PathProcessorTest {

    @Test
    void testProcess() {
        PathProcessor pathProcessor = new PathProcessor();
        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> pathProcessor.process(null)
        );
        assertTrue(thrown.getMessage().contains("Path is null"));
    }

    @Test
    void testValidatePathAddToFile() {
        PathProcessor pathProcessor = new PathProcessor();
        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> pathProcessor.process("root/file.txt/folder")
        );
        assertTrue(thrown.getMessage().contains("file cannot be added to file or folder"));
    }
}