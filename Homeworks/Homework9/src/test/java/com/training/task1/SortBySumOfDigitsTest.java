package com.training.task1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SortBySumOfDigitsTest {

    @Test
    void testSort() {
        SortBySumOfNumbers newSort = new SortBySumOfNumbers();
        Integer[] actual = newSort.sort(new Integer[]{8, 22, 101, 1});
        assertArrayEquals(new Integer[]{1, 101, 22, 8}, actual);
    }

    @Test
    void testSortNull() {
        SortBySumOfNumbers newSort = new SortBySumOfNumbers();
        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> newSort.sort(null)
        );
        assertTrue(thrown.getMessage().contains("Array is null"));
    }

}