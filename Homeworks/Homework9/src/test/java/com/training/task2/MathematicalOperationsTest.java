package com.training.task2;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class MathematicalOperationsTest {

    @Test
    void testUnion() {

        Set<String> firstSet = new HashSet<>();
        firstSet.add("A");
        firstSet.add("B");

        Set<String> secondSet = new HashSet<>();
        secondSet.add("B");
        secondSet.add("C");

        Set<String> actual = MathematicalOperations.union(firstSet, secondSet);

        Set<String> expected = new HashSet<>();
        expected.add("A");
        expected.add("B");
        expected.add("C");

        assertEquals(expected, actual);
    }

    @Test
    void testIntersection() {

        Set<String> firstSet = new HashSet<>();
        firstSet.add("A");
        firstSet.add("B");

        Set<String> secondSet = new HashSet<>();
        secondSet.add("B");
        secondSet.add("C");

        Set<String> actual = MathematicalOperations.intersection(firstSet, secondSet);

        Set<String> expected = new HashSet<>();
        expected.add("B");

        assertEquals(expected, actual);
    }

    @Test
    void testSubtraction() {

        Set<String> firstSet = new HashSet<>();
        firstSet.add("A");
        firstSet.add("B");

        Set<String> secondSet = new HashSet<>();
        secondSet.add("B");
        secondSet.add("C");

        Set<String> actual = MathematicalOperations.subtraction(firstSet, secondSet);

        Set<String> expected = new HashSet<>();
        expected.add("A");

        assertEquals(expected, actual);
    }

    @Test
    void testDifference() {

        Set<String> firstSet = new HashSet<>();
        firstSet.add("A");
        firstSet.add("B");
        firstSet.add("C");
        firstSet.add("D");

        Set<String> secondSet = new HashSet<>();
        secondSet.add("B");
        secondSet.add("C");

        Set<String> actual = MathematicalOperations.difference(firstSet, secondSet);

        Set<String> expected = new HashSet<>();
        expected.add("A");
        expected.add("D");

        assertEquals(expected, actual);
    }

    @Test
    void testUnionNull() {

        Set<String> secondSet = new HashSet<>();
        secondSet.add("B");
        secondSet.add("C");

        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> MathematicalOperations.union(null, secondSet)
        );
        assertTrue(thrown.getMessage().contains("Set is null"));
    }

}