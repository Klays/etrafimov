package com.training.task1;

import java.util.Arrays;
import java.util.Comparator;

public class SortBySumOfNumbers {

    public static final String DELIMITER = "";

    public Integer[] sort(Integer[] array) {
        if (array == null) {
            throw new IllegalArgumentException("Array is null");
        }
        Comparator<Integer> comparator = (o1, o2) -> {
            Integer first = sumOfDigits(o1);
            Integer second = sumOfDigits(o2);
            return first.compareTo(second);
        };
        Arrays.sort(array, comparator);

        return array;
    }

    public static int sumOfDigits(Integer num) {
        String number = Integer.toString(num);
        String[] numbers = number.split(DELIMITER);
        return Arrays.stream(numbers)
                .mapToInt(Integer::parseInt)
                .sum();
    }
}