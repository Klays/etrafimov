package com.training.task2;

import java.util.HashSet;
import java.util.Set;

public class MathematicalOperations {

    private MathematicalOperations() {
    }

    private static <T> void validate(Set<T> firstSet, Set<T> secondSet) {

        if (firstSet == null || secondSet == null) {
            throw new IllegalArgumentException("Set is null");
        }
    }

    public static <T> Set<T> union(Set<T> firstSet, Set<T> secondSet) {

        validate(firstSet, secondSet);

        Set<T> union = new HashSet<>(firstSet);

        union.addAll(secondSet);

        return union;
    }

    public static <T> Set<T> intersection(Set<T> firstSet, Set<T> secondSet) {

        validate(firstSet, secondSet);

        Set<T> intersection = new HashSet<>(firstSet);

        intersection.retainAll(secondSet);

        return intersection;
    }

    public static <T> Set<T> subtraction(Set<T> firstSet, Set<T> secondSet) {

        validate(firstSet, secondSet);

        Set<T> subtraction = new HashSet<>(firstSet);

        subtraction.removeAll(secondSet);

        return subtraction;
    }

    public static <T> Set<T> difference(Set<T> firstSet, Set<T> secondSet) {

        Set<T> differenceSet = union(firstSet, secondSet);

        Set<T> intersectionSet = intersection(firstSet, secondSet);

        differenceSet.removeAll(intersectionSet);

        return differenceSet;
    }

}