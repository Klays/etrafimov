package com.trainig.task1;

public class NumberG {
    /**
     * The method is used to calculate the number G.
     *
     * @return Returns the number G.
     * @throws IllegalArgumentException division by the denominator is excluded if it is zero.
     */
    public double calculateG(int numberFirst, int numberSecond, double numberThird, double numberFourth) {
        double divider = Math.pow(numberSecond, 2) * (numberThird + numberFourth);
        if (divider == 0) {
            throw new IllegalArgumentException("Error, division by zero");
        }
        return 4 * Math.pow(Math.PI, 2) * ((Math.pow(numberFirst, 3)) / divider);
    }
}
