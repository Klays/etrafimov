package com.trainig.task2;

public class Factorial {

    /**
     * Factorial calculation methods
     * using three different loops (for loop, wile loop, do wile loop).
     * @param parameter parameter passed to the algorithm.
     * @return Returns the factorial value.
     */
    public int calculateViaFor(int parameter) {
        int fact = 1;
        for (int i = 0; i < parameter; ) {
            fact = fact * parameter;
            parameter--;
        }
        return fact;
    }

    public int calculateViaWhile(int parameter) {
        int fact = 1;
        while (parameter > 0) {
            fact = fact * parameter;
            parameter--;
        }
        return fact;
    }

    public int calculateViaDoWhile(int parameter) {
        int fact = 1;
        do {
            fact = fact * parameter;
            parameter--;
        } while (parameter > 0);
        return fact;
    }
}
