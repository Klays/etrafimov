package com.trainig.task2;

public class AlgorithmChoicer {
    private final Factorial factorial = new Factorial();
    private final FibonacciNumberSeries fibonacciNumberSeries = new FibonacciNumberSeries();

    public static final int LOOP_TYPE_WHILE = 1;
    public static final int LOOP_TYPE_DO_WHILE = 2;
    public static final int LOOP_TYPE_FOR = 3;
    public static final int ALGORITHM_TYPE_FIBONACCI = 1;
    public static final int ALGORITHM_TYPE_FACTORIAL = 2;

    /**
     * The method is used to select the type of algorithms and the type of loops.
     * @param loopType      Parameter for selecting cycle types (1...3).
     * @param algorithmType parameter to select algorithm types (1, 2).
     * @param parameter     parameter passed to the algorithm.
     */
    protected void choiceTypes(int loopType, int algorithmType, int parameter) {
        if (loopType == LOOP_TYPE_WHILE) {
            if (algorithmType == ALGORITHM_TYPE_FIBONACCI) {
                fibonacciNumberSeries.calculateViaWhile(parameter);
            } else if (algorithmType == ALGORITHM_TYPE_FACTORIAL) {
                factorial.calculateViaWhile(parameter);
            }
        } else if (loopType == LOOP_TYPE_DO_WHILE) {
            if (algorithmType == ALGORITHM_TYPE_FIBONACCI) {
                fibonacciNumberSeries.calculateViaDoWhile(parameter);
            } else if (algorithmType == ALGORITHM_TYPE_FACTORIAL) {
                factorial.calculateViaDoWhile(parameter);
            }
        } else if (loopType == LOOP_TYPE_FOR) {
            if (algorithmType == ALGORITHM_TYPE_FIBONACCI) {
                fibonacciNumberSeries.calculateViaFor(parameter);
            } else if (algorithmType == ALGORITHM_TYPE_FACTORIAL) {
                factorial.calculateViaFor(parameter);
            }
        }
    }
}

