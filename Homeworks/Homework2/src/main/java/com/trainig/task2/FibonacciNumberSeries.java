package com.trainig.task2;

public class FibonacciNumberSeries {

    public static final int FIRST_ITEM_NUMBER = 0;
    public static final int SECOND_ITEM_NUMBER = 1;

    /**
     * Methods for calculating a number of Fibonacci numbers
     * using three loops (for loop, wile loop, do wile loop).
     * @param parameter parameter passed to the algorithm.
     * @return Returns fibonacci numbers.
     */
    public int[] calculateViaFor(int parameter) {
        int[] fibonacci = new int[parameter];
        fibonacci[FIRST_ITEM_NUMBER] = 0;
        fibonacci[SECOND_ITEM_NUMBER] = 1;
        for (int i = 2; i < fibonacci.length; ++i) {
            fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
        }
        return fibonacci;
    }

    public int[] calculateViaWhile(int parameter) {
        int[] fibonacci = new int[parameter];
        fibonacci[FIRST_ITEM_NUMBER] = 0;
        fibonacci[SECOND_ITEM_NUMBER] = 1;
        int i = 2;
        do {
            fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
            i++;
        } while (i < parameter);
        return fibonacci;
    }

    public int[] calculateViaDoWhile(int parameter) {
        int[] fibonacci = new int[parameter];
        fibonacci[FIRST_ITEM_NUMBER] = 0;
        fibonacci[SECOND_ITEM_NUMBER] = 1;
        int i = 2;
        while (i < parameter) {
            fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
            i++;
        }
        return fibonacci;
    }
}
