package com.trainig.task2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FibonacciNumberSeriesTest {

    @Test
    void testCalculateViaFor() {
        FibonacciNumberSeries fibonacciNumberSeries = new FibonacciNumberSeries();
        int[] result = fibonacciNumberSeries.calculateViaFor(5);
        assertArrayEquals(new int[]{0, 1, 1, 2, 3}, result);
    }

    @Test
    void testCalculateViaWhile() {
        FibonacciNumberSeries fibonacciNumberSeries = new FibonacciNumberSeries();
        int[] result = fibonacciNumberSeries.calculateViaWhile(6);
        assertArrayEquals(new int[]{0, 1, 1, 2, 3, 5}, result);
    }

    @Test
    void testCalculateViaDoWhile() {
        FibonacciNumberSeries fibonacciNumberSeries = new FibonacciNumberSeries();
        int[] result = fibonacciNumberSeries.calculateViaDoWhile(7);
        assertArrayEquals(new int[]{0, 1, 1, 2, 3, 5, 8}, result);
    }
}