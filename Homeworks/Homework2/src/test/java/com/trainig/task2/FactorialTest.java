package com.trainig.task2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FactorialTest {

    @Test
    void testCalculateViaFor() {
        Factorial factorial = new Factorial();
        int result = factorial.calculateViaDoWhile(5);
        assertEquals(120, result);
    }

    @Test
    void testCalculateViaWhile() {
        Factorial factorial = new Factorial();
        int result = factorial.calculateViaDoWhile(6);
        assertEquals(720, result);
    }

    @Test
    void testCalculateViaDoWhile() {
        Factorial factorial = new Factorial();
        int result = factorial.calculateViaDoWhile(7);
        assertEquals(5040, result);
    }
}