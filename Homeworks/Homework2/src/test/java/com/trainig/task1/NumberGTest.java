package com.trainig.task1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class NumberGTest {

    private static final double DELTA = 0.01;

    @Test
    void testCalculateG() {
        NumberG numberG = new NumberG();
        double result = numberG.calculateG(1, 2, 2.5, 3.5);
        assertEquals(1.64, result, DELTA);
    }

    @Test()
    void testCalculateGDivedByZero() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    NumberG numberG = new NumberG();
                    numberG.calculateG(1, 0, 2.5, 3.5);
                });
    }
}