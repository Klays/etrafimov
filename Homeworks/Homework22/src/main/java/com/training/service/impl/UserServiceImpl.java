package com.training.service.impl;

import com.training.Dto.LoginDto;
import com.training.dao.UserDao;
import com.training.exceptions.UserExistsException;
import com.training.model.User;
import com.training.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public Optional<User> findByLogin(String login) {
        return userDao.getByLogin(login);
    }

    @Override
    public Optional<String> login(String login) {
        if (userDao.getByLogin(login).isPresent()) {
            return Optional.ofNullable(login);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public void registerNewAccount(final LoginDto loginDto) {
        if (loginDto.getLogin().isEmpty()) {
            throw new UserExistsException("This user already exists");
        }
        final User user = new User();
        user.setLogin(loginDto.getLogin());
        user.setPassword(passwordEncoder.encode(loginDto.getPassword()));
        userDao.save(user);
    }

}
