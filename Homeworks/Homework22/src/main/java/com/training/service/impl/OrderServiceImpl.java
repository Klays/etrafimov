package com.training.service.impl;

import com.training.dao.OrdersDao;
import com.training.model.Good;
import com.training.model.Order;
import com.training.model.User;
import com.training.service.GoodService;
import com.training.service.OrderService;
import com.training.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    @Autowired
    private final OrdersDao ordersDao;
    @Autowired
    private final UserService userService;
    @Autowired
    private final GoodService goodService;

    @Override
    public void submitOrder(String login, List<Good> goods) {
        Optional<User> user = userService.findByLogin(login);
        Order order = new Order();
        order.setTotalPrice(goodService.getTotalPrice(goods));
        order.setUser(user.get());
        order.setGoods(goods);
        ordersDao.saveOrder(order);
    }

}
