package com.training.controller;

import com.training.Dto.OrderDto;
import com.training.model.Good;
import com.training.service.GoodService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequiredArgsConstructor
@SessionAttributes({"login", "orderedGoods"})
public class GoodController {
    
    private static final String ORDER_DTO = "orderDto";
    public static final String ORDERED_GOODS = "orderedGoods";

    private final GoodService goodService;

    @GetMapping("/goods")
    public ModelAndView goods(Model model, HttpSession httpSession) {
        ModelAndView modelAndView = new ModelAndView("productsPage");
        List<Good> goods = (ArrayList<Good>) model.getAttribute(ORDERED_GOODS);
        modelAndView.addObject(ORDERED_GOODS, goods);
        modelAndView.addObject("totalPrice", goodService.getTotalPrice(goods));
        modelAndView.addObject(ORDER_DTO, new OrderDto());
        modelAndView.addObject("goods", goodService.findAll());
        return modelAndView;
    }

    @PostMapping("/goods")
    public String goodsCatalog(@ModelAttribute(ORDER_DTO) OrderDto orderDto,
                               Model model) {

        List<Good> goods = (ArrayList<Good>) model.getAttribute(ORDERED_GOODS);
        model.addAttribute(ORDERED_GOODS, goodService.addGoodToCart(orderDto, goods));
        return "redirect:/goods";

    }
}

