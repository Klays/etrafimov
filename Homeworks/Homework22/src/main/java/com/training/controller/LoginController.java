package com.training.controller;

import com.training.Dto.LoginDto;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Scope("session")
@Controller
@RequiredArgsConstructor
@SessionAttributes("login")
public class LoginController {

    private static final String REDIRECT_LOGIN = "redirect:/login";
    private static final String LOGIN_DTO = "loginDto";
    private static final String REDIRECT_GOODS = "redirect:/goods";

    @GetMapping("/login")
    public ModelAndView loginPage(final Authentication authentication) {
        final ModelAndView modelAndView = new ModelAndView();
        if (authentication == null) {
            modelAndView.addObject(LOGIN_DTO, new LoginDto());
            modelAndView.setViewName("index");
        } else {
            modelAndView.setViewName(REDIRECT_GOODS);
        }

        return modelAndView;
    }

    @PostMapping("/login")
    public String login(@ModelAttribute(LOGIN_DTO) LoginDto loginDto) {
        return REDIRECT_GOODS;
    }

    @GetMapping({"", "/"})
    public String showHomePage() {
        return REDIRECT_LOGIN;
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }
        return REDIRECT_LOGIN;
    }
}
