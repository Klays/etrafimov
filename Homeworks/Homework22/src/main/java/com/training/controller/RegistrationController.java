package com.training.controller;

import com.training.Dto.LoginDto;
import com.training.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Scope("session")
@Controller
@RequiredArgsConstructor
public class RegistrationController {

    private final UserService userService;

    @GetMapping("/registration")
    public ModelAndView registration() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("loginDto", new LoginDto());
        modelAndView.setViewName("registration");
        return modelAndView;
    }

    @PostMapping("/registration")
    public String processForm(@Valid @ModelAttribute final LoginDto loginDto,
                              final BindingResult bindingResult,
                              final RedirectAttributes attributes) {

        if (bindingResult.hasErrors()) {
            return "/registration";
        }

        if (loginDto != null) {
            userService.registerNewAccount(loginDto);
        }

        return "redirect:/login";
    }

}
