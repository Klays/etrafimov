package com.training.dao;

import com.training.model.Good;

import java.util.List;

public interface GoodsDao {

    List<Good> getUserOrders(String login);

    List<Good> getAll();

    Good getById(Long id);

}
