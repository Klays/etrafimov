package com.training.dao.impl;

import com.training.dao.GoodsDao;
import com.training.model.Good;
import com.training.util.HibernateUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import java.util.List;

@Slf4j
@Repository
@RequiredArgsConstructor
public class GoodsDaoIml implements GoodsDao {

    private static final String GET_GOOD_QUERY = "FROM Good g WHERE g.id = :id";

    private static final String SELECT_ALL = "FROM Good";

    private static final String SELECT_USER_ORDERS = "SELECT id, title, price FROM Good g" +
            "inner join orders_goods og " +
            "inner join orders o " +
            "inner join users u " +
            "where u.login = :login";

    @Override
    public List<Good> getAll() {
        Transaction transaction = null;
        List<Good> goods = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            goods = session.createQuery(SELECT_ALL, Good.class).getResultList();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            log.info("Search error");
        }
        return goods;
    }

    @Override
    public List<Good> getUserOrders(String login) {
        Transaction transaction = null;
        List<Good> goods = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            goods = session.createQuery(SELECT_USER_ORDERS, Good.class).setParameter("login", login).getResultList();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            log.info("Good not found");
        }
        return goods;
    }

    @Override
    public Good getById(Long id) {
        Transaction transaction = null;
        Good good = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            transaction = session.beginTransaction();

            good = session.createQuery(GET_GOOD_QUERY, Good.class)
                    .setParameter("id", id)
                    .uniqueResult();

            transaction.commit();
        } catch (HibernateException exception) {
            if (transaction != null) {
                transaction.rollback();
            }
            log.info("Good not found");
        }
        return good;
    }
}
