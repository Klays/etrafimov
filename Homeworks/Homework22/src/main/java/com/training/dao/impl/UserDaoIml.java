package com.training.dao.impl;

import com.training.dao.UserDao;
import com.training.model.User;
import com.training.util.HibernateUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Slf4j
@Repository
@RequiredArgsConstructor
public class UserDaoIml implements UserDao {

    private static final String SELECT_BY_LOGIN = "FROM User u WHERE u.login = :login";

    private static final String GET_USER_BY_ID_QUERY = "FROM User u WHERE u.id = :id";

    private static final String SELECT_ALL = "FROM User";

    @Override
    public Optional<User> getByLogin(String login) {

        Transaction transaction = null;
        User user = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            user = session.createQuery(SELECT_BY_LOGIN, User.class)
                    .setParameter("login", login)
                    .uniqueResult();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            log.info("User not found");
        }
        return Optional.ofNullable(user);
    }

    @Override
    public void save(User user) {

        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.save(user);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            log.info("Save error");
        }
    }

    @Override
    public List<User> getAll() {

        Transaction transaction = null;
        List<User> user = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            user = session.createQuery(SELECT_ALL, User.class).getResultList();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            log.info("Search error");
        }
        return user;
    }

    @Override
    public User getById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Invalid user id");
        }
        Transaction transaction = null;
        User user = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            transaction = session.beginTransaction();

            user = session.createQuery(GET_USER_BY_ID_QUERY, User.class).uniqueResult();

            transaction.commit();
        } catch (HibernateException exception) {
            if (transaction != null) {
                transaction.rollback();
            }
            log.info("Search error");
        }
        return user;
    }

}
