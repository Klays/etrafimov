package com.training.dao;

import com.training.model.User;

import java.util.List;
import java.util.Optional;

public interface UserDao {

    Optional<User> getByLogin(String login);

    void save(User user);

    List<User> getAll();

    User getById(Long id);

}
