package com.training.exceptions;

import org.springframework.security.core.AuthenticationException;

public class UserExistsException extends AuthenticationException{

    public UserExistsException(String message) {
        super(message);
    }
}
