package com.training.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
public class Good {

    private Long id;
    private String name;
    private BigDecimal price;

    @Override
    public String toString() {
        return String.format("%s %s$", getName(), getPrice());
    }
}
