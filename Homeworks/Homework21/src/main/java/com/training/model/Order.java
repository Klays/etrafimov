package com.training.model;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class Order {

    private Long id;
    private Long userId;
    private BigDecimal totalPrice;

}
