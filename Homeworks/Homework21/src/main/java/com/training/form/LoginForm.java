package com.training.form;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class LoginForm {

    private String login;

    private String password;

    private String passwordConfirm;
}
