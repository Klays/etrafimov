package com.training.controller;

import com.training.form.LoginForm;
import com.training.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Scope("session")
@Controller
@RequiredArgsConstructor
@SessionAttributes("login")
public class LoginController {

    public static final String LOGIN = "/login";
    public static final String REDIRECT_LOGIN = "redirect:/login";
    public static final String LOGIN_FORM = "loginForm";
    private final UserService userService;

    @GetMapping(LOGIN)
    public ModelAndView loginPage(final Authentication authentication) {
        final ModelAndView modelAndView = new ModelAndView();
        if (authentication == null) {
            modelAndView.addObject(LOGIN_FORM, new LoginForm());
            modelAndView.setViewName("index");
        }else {
            modelAndView.setViewName(REDIRECT_LOGIN);
        }

        return modelAndView;
    }

    @PostMapping(LOGIN)
    public String login(@ModelAttribute(LOGIN_FORM) LoginForm loginForm, HttpSession httpSession,
                        @RequestParam(value = "terms", required = false) String checkboxValue) {
        /*if (checkboxValue != null) {
            httpSession.setAttribute("login", loginForm.getLogin());
            userService.login(loginForm.getLogin());
            return "redirect:/goods";
        } else {
            return "termsErrorPage";
        }*/
        return "redirect:/goods";
    }

    @GetMapping({"", "/"})
    public String showHomePage() {
        return "redirect:/login";
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }
        return REDIRECT_LOGIN;
    }
}
