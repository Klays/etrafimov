package com.training.controller.REG;

import com.training.form.LoginForm;
import com.training.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Scope("session")
@Controller
@RequiredArgsConstructor
public class RegistrationController {

    private final UserService userService;

    @GetMapping("/registration")
    public ModelAndView registration() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("loginForm", new LoginForm());
        modelAndView.setViewName("registration");
        return modelAndView;
    }


    @PostMapping("/registration")
    public String processForm(@Valid @ModelAttribute final LoginForm loginForm,
                              final BindingResult bindingResult,
                              final RedirectAttributes attributes) {

        if (bindingResult.hasErrors()) {
            return "/registration";
        }

        if (loginForm != null) {
            userService.registerNewAccount(loginForm);
        }

        return "redirect:/login";
    }








   /* @PostMapping
    public String registerUserAccount(@ModelAttribute("user") @Valid UserDto userDto,
                                      BindingResult result) {

        Optional<User> existing = userService.findByLogin(userDto.getLogin());
        if (existing.isPresent()) {
            result.rejectValue("email", null, "There is already an account registered with that email");
        }

        if (result.hasErrors()) {
            return "registration";
        }

        userService.save(userDto);
        return "redirect:/registration";
    }*/




   /* @PostMapping("/registration")
    public String addUser(@ModelAttribute("userDto") UserDto userDto, User user,
                          BindingResult bindingResult) {
        *//*probnik
        if (userService.findByLogin(user.getLogin()).isPresent()) {
            model.addAttribute("username", "Duplicate.userForm.username");
        }/*probnik*//*


        if (bindingResult.hasErrors()) {
            return "redirect:/registration";
        }
        if (user != null) {
            userService.registerNewUserAccount(userDto);

        }

        return "redirect:/login";
    }*/



/*if (userForm == null) {
        model.addAttribute("passwordOrLoginError", "ERROR REG");
        return "registration";
    }
        if (bindingResult.hasErrors()) {
        model.addAttribute("error", "ERROR REG");
        return "registration";
    }*/






 /*
    @PostMapping("/registration")
    public String addUser(User user, Model model) {
        Optional<User> userFromDb = userService.findByLogin(user.getLogin());

        if (userFromDb.isPresent()) {
            model.addAttribute("loginError", "User exists");
            return "registration";
        }

        userService.findByLogin(user.getLogin());
        return "redirect:/login";
    }*/








    /*
    @PostMapping("/registration")
    public String addUser(@ModelAttribute("loginForm") LoginForm loginForm,
                          BindingResult bindingResult, Model model) {
        ModelAndView modelAndView = new ModelAndView("registration");
        modelAndView.addObject("loginForm", new LoginForm());

        if (bindingResult.hasErrors()) {
            return "registration";
        }
        if (!loginForm.getPassword().equals(loginForm.getPasswordConfirm())) {
            model.addAttribute("passwordError", "Пароли не совпадают");
            return "registration";
        }

        if (!userService.login(loginForm.getLogin()).isPresent()) {
            model.addAttribute("usernameError", "Пользователь с таким именем уже существует");
            return "registration";
        }

        return "redirect:/login";
    }*/

}
