package com.training.controller;

import com.training.model.Good;
import com.training.model.UserPrincipal;
import com.training.service.GoodService;
import com.training.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import java.util.ArrayList;
import java.util.List;

import static com.training.controller.GoodController.ORDERED_GOODS;

@Scope("session")
@Controller
@RequiredArgsConstructor
@SessionAttributes({"orderedGoods"})
public class OrderController {

    public static final String ORDERS = "/orders";
    private final GoodService goodService;

    private final OrderService orderService;

    @PostMapping(ORDERS)
    public String submitOrder(Model model, Authentication authentication) {
        String loginUser =  ((UserPrincipal)authentication.getPrincipal()).getUser().getLogin();
        List<Good> goods = (ArrayList<Good>) model.getAttribute(ORDERED_GOODS);
        orderService.submitOrder(loginUser, goods);

        return "redirect:/orders";
    }

    @GetMapping(ORDERS)
    public String showOrder(Model model) {
        String login = (String) model.getAttribute("login");
        List<Good> goodList = goodService.getUserOrders(login);
        model.addAttribute(ORDERS, goodList);
        model.addAttribute("totalPrice", goodService.getTotalPrice(goodList));
        return "orderPage";
    }


}
