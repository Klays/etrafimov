package com.training.controller;

import com.training.form.OrderForm;
import com.training.model.Good;
import com.training.service.GoodService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Scope("session")
@Controller
@RequiredArgsConstructor
@SessionAttributes({"login", "orderedGoods"})
public class GoodController {

    public static final String GOODS = "/goods";
    public static final String ORDER_FORM = "orderForm";
    private final GoodService goodService;

    public static final String ORDERED_GOODS = "orderedGoods";

    @GetMapping(GOODS)
    public ModelAndView goods(Model model, HttpSession httpSession) {
        ModelAndView modelAndView = new ModelAndView("productsPage");
        List<Good> goods = (ArrayList<Good>) model.getAttribute(ORDERED_GOODS);
        modelAndView.addObject(ORDERED_GOODS, goods);
        modelAndView.addObject("totalPrice", goodService.getTotalPrice(goods));
        modelAndView.addObject(ORDER_FORM, new OrderForm());
        modelAndView.addObject("goods", goodService.findAll());
        return modelAndView;
    }

    @PostMapping(GOODS)
    public String goodsCatalog(@ModelAttribute(ORDER_FORM) OrderForm orderForm,
                               Model model) {

        List<Good> goods = (ArrayList<Good>) model.getAttribute(ORDERED_GOODS);
        model.addAttribute(ORDERED_GOODS, goodService.addGoodToCart(orderForm, goods));
        return "redirect:/goods";

    }
}

