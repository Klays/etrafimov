package com.training.exceptions;

import org.springframework.security.core.AuthenticationException;

public class UserExist extends AuthenticationException{


    public UserExist(String message) {
        super(message);
    }
}
