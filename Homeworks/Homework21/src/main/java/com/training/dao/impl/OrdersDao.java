package com.training.dao.impl;

import com.training.connection.ConnectionDb;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Objects;

@Repository
@RequiredArgsConstructor
public class OrdersDao {

    private static final String SQL_INSERT_ORDER = "INSERT INTO orders (user_id, total_price) VALUES(?,?)";
    private static final String SQL_INSERT_ORDER_GOOD = "INSERT INTO orders_goods (order_id, good_id) VALUES(?,?)";

    private final ConnectionDb connectionDb;

    @SneakyThrows
    public Long insertOrder(Long userId, BigDecimal totalPrice) {
        try (PreparedStatement statement = Objects.requireNonNull(connectionDb.getConnection())
                .prepareStatement(SQL_INSERT_ORDER, Statement.RETURN_GENERATED_KEYS)) {
            statement.setLong(1, userId);
            statement.setBigDecimal(2, totalPrice);
            statement.executeUpdate();
            try (ResultSet resultSet = statement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    return resultSet.getLong(1);
                }
            }
        }
        return null;
    }

    @SneakyThrows
    public Long insertOrderGoods(Long orderId, Long goodId) {
        try (PreparedStatement statement = Objects.requireNonNull(connectionDb.getConnection())
                .prepareStatement(SQL_INSERT_ORDER_GOOD, Statement.RETURN_GENERATED_KEYS)) {
            statement.setLong(1, orderId);
            statement.setLong(2, goodId);
            statement.executeUpdate();
            Long resultSet = getaLong(statement);
            if (resultSet != null) return resultSet;
        }
        return null;
    }

    private Long getaLong(PreparedStatement statement) throws SQLException {
        try (ResultSet resultSet = statement.getGeneratedKeys()) {
            if (resultSet.next()) {
                return resultSet.getLong(1);
            }
        }
        return null;
    }

}
