package com.training.service.impl;

import com.training.model.User;
import com.training.model.UserPrincipal;
import com.training.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@RequiredArgsConstructor
@Service
public class UserInformation implements UserDetailsService {

    private final UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final User user = userService.findByLogin(username)
                .orElseThrow(() -> new UsernameNotFoundException(
                        "User not found")
                );

        return new UserPrincipal(user);
    }
}
