package com.training.service;

import com.training.form.LoginForm;
import com.training.model.User;

import java.util.Optional;

public interface UserService {

    Optional<User> findByLogin(String login);

    Optional<String> login(String login);

    void registerNewAccount(LoginForm loginForm);

}
