package com.training.service;

import com.training.model.Good;

import java.math.BigDecimal;
import java.util.List;

public interface OrderService {

    Long insertOrder(Long userId, BigDecimal totalPrice);

    Long insertOrderGoods(Long orderId, Long goodId);

    void submitOrder(String login, List<Good> goods);

}
