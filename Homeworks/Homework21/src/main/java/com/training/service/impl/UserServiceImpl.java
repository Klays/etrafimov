package com.training.service.impl;

import com.training.dao.impl.UserDao;
import com.training.exceptions.UserExist;
import com.training.form.LoginForm;
import com.training.model.User;
import com.training.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private  UserDao userDao;

    @Autowired
    private  BCryptPasswordEncoder passwordEncoder;

    @Override
    public Optional<User> findByLogin(String login) {
        return userDao.getByLogin(login);
    }

    @Override
    public Optional<String> login(String login) {
        if (userDao.getByLogin(login).isPresent()) {
            return Optional.ofNullable(login);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public void registerNewAccount(final LoginForm loginForm) {
        if (loginForm.getLogin().isEmpty()) {
            throw new UserExist("This user already exists");
        }
        final User user = new User();
        user.setLogin(loginForm.getLogin());
        user.setPassword(passwordEncoder.encode(loginForm.getPassword()));
        userDao.save(user);
    }

 /*   @Transactional
    public <S extends T> S save(S entity) {
        if (entityInformation.isNew(entity)) {
            em.persist(entity);
            return entity;
        } else {
            return em.merge(entity);
        }
    }*/
}
