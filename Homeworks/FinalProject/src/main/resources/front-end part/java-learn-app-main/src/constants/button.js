
const actionMap = new Map();

actionMap.set(
  "SUBMIT",
    "NEW"
);
actionMap.set(
  "APPROVE",
  "APPROVED"
);
actionMap.set(
  "CANCEL",
  "CANCELED"
);
actionMap.set(
  "COMPLETE",
  "DONE"
);
actionMap.set(
  "LEAVE_FEEDBACK",
  "LEAVE_FEEDBACK"
);
actionMap.set(
  "DECLINE",
  "DECLINED"
);
actionMap.set(
  "ASSIGN_TO_ME",
  "IN_PROGRESS"
);

export default actionMap;
