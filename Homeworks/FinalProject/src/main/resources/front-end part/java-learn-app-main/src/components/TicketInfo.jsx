import React from "react";
import PropTypes from "prop-types";
import CommentsTable from "./CommentsTable";
import HistoryTable from "./HistoryTable";
import TabPanel from "./TabPanel";
import TicketCreationPageWithRouter from "./TicketCreationPage";
import { Link, Route, Switch } from "react-router-dom";
import { withRouter } from "react-router";
import API from "../API";
import CustomButton from "./CustomButton";

import {
  Button,
  ButtonGroup,
  Paper,
  Tab,
  Tabs,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Typography,
  TextField,
} from "@material-ui/core";

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    "aria-controls": `full-width-tabpanel-${index}`,
  };
}

class TicketInfo extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      commentValue: "",
      tabValue: 0,
      ticketComments: [],
      ticketHistory: [],
      currentUser: {
        name: "Dave Brubeck",
        id: 4242,
      },
      ticketData: {
        id: 42,
        name: "Something",
        date: "2021-07-16",
        category: "Hardware & Software",
        status: "New",
        urgency: "High",
        resolutionDate: "",
        ticketOwner: "Robert Oppenheimer",
        approver: "",
        assignee: "",
        actions: [],
        attachment: null,
        description: "Desc",
      },
    };
  }

  componentDidMount() {
    // get required ticket by id
    const { ticketId } = this.props.match.params;
    var closure = this;
    API.get("/tickets/" + ticketId).then(function (response) {
      console.log(response);
      closure.setState({
        ticketData: {
          ...closure.state.ticketData,
          id: response.data.id,
          date: response.data.createdOn,
          resolutionDate: response.data.desiredResolutionDate,
          name: response.data.name,
          status: response.data.state,
          urgency: response.data.urgency,
          category: response.data.category,
          ticketOwner: response.data.owner,
          assignee: response.data.assignee,
          approver: response.data.approver,
          actions: response.data.actions,
        },
      });
    });

    API.get("/ticket/" + ticketId + "/comments").then(function (response) {
      closure.state.ticketComments = response.data;
    });

    API.get("/tickets/" + ticketId + "/histories").then(function (response) {
      closure.state.ticketHistory = response.data;
    });
  }

  handleTabChange = (event, value) => {
    this.setState({
      tabValue: value,
    });
  };

  handleEnterComment = (event) => {
    this.setState({
      commentValue: event.target.value,
    });
  };

  addComment = () => {
    // put request for comment creation here
    var request = {
      text: this.state.commentValue,
    };

    API.post("/ticket/" + this.state.ticketData.id + "/comments", request).then(
      function (responce) {
        window.location.reload();
      }
    );

    const newComment = {
      date: new Date().toLocaleDateString(),
      user: this.state.currentUser.name,
      comment: this.state.commentValue,
    };
    this.setState({
      ticketComments: [...this.state.ticketComments, newComment],
      commentValue: "",
    });
  };

  handleEditTicket = () => {
    console.log("EDIT ticket");
  };

  render() {
    const {
      approver,
      actions,
      id,
      name,
      date,
      category,
      status,
      urgency,
      resolutionDate,
      ticketOwner,
      assignee,
      attachment,
      description,
    } = this.state.ticketData;

    const { commentValue, tabValue, ticketComments, ticketHistory } =
      this.state;

    const { url } = this.props.match;

    const { handleCancelTicket, handleEditTicket, handleSubmitTicket } = this;

    return (
      <Switch>
        <Route exact path={url}>
          <div className="ticket-data-container">
            <div className={"ticket-data-container__back-button back-button"}>
              <Button component={Link} to="/main-page" variant="contained">
                Ticket list
              </Button>
              <Button
                component={Link}
                to={"/create-ticket/" + id}
                variant="contained"
              >
                Edit
              </Button>
            </div>
            <div className="ticket-data-container__title">
              <Typography variant="h4">{`Ticket(${id}) - ${name}`}</Typography>
            </div>
            <div className="ticket-data-container__info">
              <TableContainer className="ticket-table" component={Paper}>
                <Table>
                  <TableBody>
                    <TableRow>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          Created on:
                        </Typography>
                      </TableCell>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          {date}
                        </Typography>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          Category:
                        </Typography>
                      </TableCell>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          {category.name}
                        </Typography>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          Status:
                        </Typography>
                      </TableCell>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          {status}
                        </Typography>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          Urgency:
                        </Typography>
                      </TableCell>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          {urgency}
                        </Typography>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          Desired Resolution Date:
                        </Typography>
                      </TableCell>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          {resolutionDate}
                        </Typography>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          Owner:
                        </Typography>
                      </TableCell>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          {ticketOwner.email}
                        </Typography>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          Approver:
                        </Typography>
                      </TableCell>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          {approver !== null ? approver.email : "Not assigned"}
                        </Typography>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          Assignee:
                        </Typography>
                      </TableCell>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          {assignee !== null ? assignee.email : "Not assigned"}
                        </Typography>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          Attachments:
                        </Typography>
                      </TableCell>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          {attachment || "Not assigned"}
                        </Typography>
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          Description:
                        </Typography>
                      </TableCell>
                      <TableCell>
                        <Typography align="left" variant="subtitle1">
                          {description || "Not assigned"}
                        </Typography>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
            </div>
            {
              <div className="ticket-data-container__button-section">
                <ButtonGroup>
                  {actions !== null
                    ? actions.map((action) => {
                        return (
                          <CustomButton id={id} action={action} key={action} />
                        );
                      })
                    : ""}
                </ButtonGroup>
              </div>
            }
            <div className="ticket-data-container__comments-section comments-section">
              <div className="">
                <Tabs
                  variant="fullWidth"
                  onChange={this.handleTabChange}
                  value={tabValue}
                  indicatorColor="primary"
                  textColor="primary"
                >
                  <Tab label="History" {...a11yProps(0)} />
                  <Tab label="Comments" {...a11yProps(1)} />
                </Tabs>
                <TabPanel value={tabValue} index={0}>
                  <HistoryTable history={ticketHistory} />
                </TabPanel>
                <TabPanel value={tabValue} index={1}>
                  <CommentsTable comments={ticketComments} />
                </TabPanel>
              </div>
            </div>
            {tabValue && (
              <div className="ticket-data-container__enter-comment-section enter-comment-section">
                <TextField
                  label="Enter a comment"
                  multiline
                  rows={4}
                  value={commentValue}
                  variant="filled"
                  className="comment-text-field"
                  onChange={this.handleEnterComment}
                />
                <div className="enter-comment-section__add-comment-button">
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={this.addComment}
                  >
                    Add Comment
                  </Button>
                </div>
              </div>
            )}
          </div>
        </Route>
        <Route path="/create-ticket/:ticketId">
          <TicketCreationPageWithRouter />
        </Route>
      </Switch>
    );
  }
}

TicketInfo.propTypes = {
  match: PropTypes.object,
};

const TicketInfoWithRouter = withRouter(TicketInfo);
export default TicketInfoWithRouter;
