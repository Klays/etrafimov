import React from "react";
import API from "../API";
import { Button, TextField, Typography } from "@material-ui/core";
import actionMap from "../constants/button";

class CustomButton extends React.Component {
  constructor(props) {
    super(props);
  }

  handleSubmit = () => {
    if (this.props.action !== "LEAVE_FEEDBACK") {
      API.post(
        "/tickets/" +
          this.props.id +
          "/update-status/" +
          actionMap.get(this.props.action)
      ).then(function () {
        window.location.reload();
      });
    }
  };

  render() {
    const { id, action } = this.props;
    const { handleSubmit } = this;
    return (
      <Button onClick={handleSubmit} variant="contained" color="secondary">
        {action}
      </Button>
    );
  }
}
export default CustomButton;
