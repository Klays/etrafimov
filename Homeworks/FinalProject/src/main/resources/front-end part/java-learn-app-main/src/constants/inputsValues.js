export const URGENCY_OPTIONS = [
  { value: "CRITICAL", label: "Critical" },
  { value: "HIGH", label: "High" },
  { value: "AVERAGE", label: "Average" },
  { value: "LOW", label: "Low" },
];


