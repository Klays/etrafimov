import axios from "axios";

var newAxios = axios.create({
  baseURL: "http://localhost:8080/",
  responseType: "json"
});

newAxios.interceptors.request.use(request => {
  const isLoggedIn = localStorage.getItem("Token");
  if (isLoggedIn !== null && isLoggedIn !== undefined) {
      request.headers.common.Authorization = `Bearer ${localStorage.getItem("Token")}`;
  }

  return request;
});

export default newAxios;



