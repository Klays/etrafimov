package com.training;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) {

        Card card = new Card();


        int numberOfAtmsForWithdrawal = Utils.random.nextInt((int) (3 + (Math.random() * 3)));

        int numberOfAtmsForTopUp = Utils.random.nextInt((int) (3 + (Math.random() * 3)));

        ExecutorService executorService = Executors.newFixedThreadPool(numberOfAtmsForWithdrawal + numberOfAtmsForTopUp);

        for (int i = 0; i < numberOfAtmsForWithdrawal; i++) {
            MoneyConsumer moneyConsumer = new MoneyConsumer();
            moneyConsumer.setCard(card);
            executorService.execute(moneyConsumer);
        }

        for (int i = 0; i < numberOfAtmsForTopUp; i++) {
            MoneyProducer moneyProducer = new MoneyProducer();
            moneyProducer.setCard(card);
            executorService.execute(moneyProducer);
        }
    }
}