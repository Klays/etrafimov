package com.training;

import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

public class Utils {

    public static final AtomicBoolean stop = new AtomicBoolean(true);

    public static final Random random = new Random();
}
