package com.training;

import java.math.BigDecimal;

public class MoneyConsumer extends Thread {

    private Card card;

    public void setCard(Card card) {
        this.card = card;
    }

    public static final BigDecimal THOUSAND = new BigDecimal(1000);

    @Override
    public void run() {
        try {
            while (Utils.stop.get()) {
                Thread.sleep(Utils.random.nextInt(((5 - 2) + 1) + 2) * 1000);
                if (card.getBalance().compareTo(BigDecimal.ZERO) <= 0) {
                    Utils.stop.set(false);
                } else if (card.getBalance().compareTo(THOUSAND) == 1) {
                    Utils.stop.set(false);
                } else {
                    BigDecimal cash = BigDecimal.valueOf(Utils.random.nextInt((10 - 5) + 1) + 5);
                    System.out.println("withdrawal: " + cash);
                    card.withdraw(cash);
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
