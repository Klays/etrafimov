package com.training;

import java.math.BigDecimal;

public class Card {

    protected BigDecimal balance = BigDecimal.valueOf(500);

    public BigDecimal getBalance() {
        return balance;
    }

    protected synchronized BigDecimal topUpBalance(BigDecimal cash) {
        this.balance = this.balance.add(cash);
        System.out.println("Current balance: " + balance);
        return balance;
    }

    protected synchronized BigDecimal withdraw(BigDecimal withdrawCash) {
        this.balance = this.balance.subtract(withdrawCash);
        System.out.println("Current balance: " + balance);
        return balance;
    }
}
