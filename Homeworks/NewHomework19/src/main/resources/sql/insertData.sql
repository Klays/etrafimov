INSERT INTO goods(title, price)
SELECT 'Pen', 1.0 WHERE NOT EXISTS(SELECT 2 FROM goods WHERE title='Pen');

INSERT INTO goods(title, price)
SELECT 'Table', 99.99 WHERE NOT EXISTS(SELECT 2 FROM goods WHERE title='Table');

INSERT INTO goods(title, price)
SELECT 'Mobile Phone', 1499.99 WHERE NOT EXISTS(SELECT 2 FROM goods WHERE title='Mobile Phone');

INSERT INTO goods(title, price)
SELECT 'Doll', 5.2 WHERE NOT EXISTS(SELECT 2 FROM goods WHERE title='MDoll');

INSERT INTO goods(title, price)
SELECT 'Chair', 20.15 WHERE NOT EXISTS(SELECT 2 FROM goods WHERE title='Chair');

INSERT INTO goods(title, price)
SELECT 'Sweet', 0.50 WHERE NOT EXISTS(SELECT 2 FROM goods WHERE title='Sweet');

INSERT INTO goods(title, price)
SELECT 'Tesla', 60000.49 WHERE NOT EXISTS(SELECT 2 FROM goods WHERE title='Tesla');

INSERT INTO users(id, login, password)
SELECT 1, 'admin', '' WHERE NOT EXISTS(SELECT 2 FROM users WHERE login='admin');

