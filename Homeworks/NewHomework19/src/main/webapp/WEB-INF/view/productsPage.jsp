<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="java.util.ArrayList,com.training.model.Good" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<body>
<div style="text-align: center">
    <form:form method="post" modelAttribute="orderForm">
    <h1> Hello ${login}!</h1>

    <c:if test="${not empty orderedGoods}">
        <h3>You have already chosen:</h3>

        <c:forEach var="item" items="${orderedGoods}">
            <c:out value="${item}"/>
            <br>
        </c:forEach>

        <div class="text-right">
            <h3>Order Total: ${totalPrice}$</h3>
        </div>
    </c:if>

    <label> Make your order </label>
    <form:select path="goodId">
        <form:option value="">Select Product</form:option>
        <form:options items="${goods}" itemValue="id"/>
    </form:select>
    <div style="text-align: center">
        <form:button type="submit"
                     value="add">Add item
        </form:button>
        <form:button type="submit"
                     formaction="${pageContext.request.contextPath}/orders"
                     value="submit">Submit
        </form:button>
        </form:form>
    </div>
</div>
</body>
</html>