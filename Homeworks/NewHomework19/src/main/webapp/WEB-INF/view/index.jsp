<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<body>
<div style="text-align: center">
    <h1>Welcome to online shop</h1>
    <br>
    <form:form method="post" modelAttribute="loginForm">
        <form:input path="login" id="login"
                    placeholder="Enter your name"/>
        <br><br/>

        <input type="checkbox" name="terms" required> I agree with the terms of service
        <br><br/>

        <button type="submit" formnovalidate> Enter</button>
    </form:form>
</div>
</body>
</html>