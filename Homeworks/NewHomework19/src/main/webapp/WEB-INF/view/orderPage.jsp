<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<body>
<div style="text-align: center">
    <h1>
        Dear ${login}, your order:
    </h1>
    <ul>
        <c:forEach var="item" items="${orders}">
            <c:out value="${item}"/>
            <br>
        </c:forEach>

        <div class="text-right">
            <h3>Order Total: ${totalPrice}$</h3>
        </div>
    </ul>
</div>
</body>
</html>
