package com.training.dao.impl;

import com.training.connection.ConnectionDb;
import com.training.dao.GoodsDao;
import com.training.model.Good;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Repository
@RequiredArgsConstructor
public class GoodsDaoIml implements GoodsDao {
    private static final String SQL_SELECT_ALL = "SELECT * FROM goods";
    private static final String SQL_GET_BY_ID = "SELECT * FROM goods WHERE id=?";
    private static final String SQL_SELECT_USER_ORDERS = "select * from goods as g\n" +
            "\tjoin orders_goods as og on g.id = og.good_id\n" +
            "\tjoin orders as o on og.order_id = o.id\n" +
            "\tjoin users as u on o.user_id = u.id\n" +
            "\t\twhere u.login = ?";


    private final ConnectionDb connectionDb;

    private void extracted(List<Good> products, PreparedStatement statement) throws SQLException {
        try (ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                products.add(parseSet(resultSet));
            }
        }
    }

    @SneakyThrows
    @Override
    public List<Good> getAll() {
        List<Good> products = new ArrayList<>();
        try (PreparedStatement statement = Objects.requireNonNull(connectionDb.getConnection())
                .prepareStatement(SQL_SELECT_ALL)) {
            extracted(products, statement);
        }
        return products;
    }

    @SneakyThrows
    @Override
    public List<Good> getUserOrders(String login) {
        List<Good> products = new ArrayList<>();
        try (PreparedStatement statement = Objects.requireNonNull(connectionDb.getConnection())
                .prepareStatement(SQL_SELECT_USER_ORDERS)) {
            statement.setString(1, login);
            extracted(products, statement);
        }
        return products;
    }

    private Good parseSet(ResultSet resultSet) throws SQLException {
        Long productId = resultSet.getLong(1);
        String name = resultSet.getString(2);
        BigDecimal price = resultSet.getBigDecimal(3);
        return new Good(productId, name, price);
    }

    @SneakyThrows
    public Good getById(Long id) {
        try (PreparedStatement statement = Objects.requireNonNull(connectionDb.getConnection()).prepareStatement(SQL_GET_BY_ID)) {
            statement.setString(1, String.valueOf(id));
            try (ResultSet resultSet = statement.executeQuery()) {
                resultSet.next();
                return parseSet(resultSet);
            }
        }
    }
}
