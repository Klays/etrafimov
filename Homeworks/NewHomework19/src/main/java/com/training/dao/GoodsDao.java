package com.training.dao;

import com.training.model.Good;

import java.util.List;

public interface GoodsDao extends BaseDao{

    List<Good> getUserOrders(String login);

}
