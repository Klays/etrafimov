package com.training.dao.impl;

import com.training.connection.ConnectionDb;
import com.training.dao.BaseDao;
import com.training.model.User;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class UserDao implements BaseDao<User, Long> {

    private static final String SQL_SELECT_BY_LOGIN = "SELECT * FROM users WHERE login = ?";
    private static final String SQL_SELECT_ALL = "SELECT * FROM users";

    private final ConnectionDb connectionDb;

    @SneakyThrows
    public Optional<User> getByLogin(String login) {
        try (PreparedStatement statement = Objects.requireNonNull(connectionDb.getConnection()).prepareStatement(SQL_SELECT_BY_LOGIN)) {
            statement.setString(1, login);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(parseSet(resultSet));
                }
            }
        }
        return Optional.empty();
    }

    @SneakyThrows
    @Override
    public List<User> getAll() {
        List<User> users = new ArrayList<>();
        try (PreparedStatement statement = Objects.requireNonNull(connectionDb.getConnection()).prepareStatement(SQL_SELECT_ALL)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    users.add(parseSet(resultSet));
                }
            }
        }
        return users;
    }

    private User parseSet(ResultSet resultSet) throws SQLException {
        Long userId = resultSet.getLong(1);
        String login = resultSet.getString(2);
        String password = resultSet.getString(3);
        return new User(userId, login, password);
    }

    @Override
    public User getById(Long id) {
        throw new UnsupportedOperationException();
    }
}
