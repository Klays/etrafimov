package com.training.service.impl;

import com.training.dao.impl.UserDao;
import com.training.model.User;
import com.training.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserDao userDao;

    @Override
    public Optional<User> findByLogin(String login) {
        return userDao.getByLogin(login);
    }

    @Override
    public Optional<String> login(String login) {
        if (findByLogin(login).isPresent()) {
            return Optional.ofNullable(login);
        } else {
            return Optional.empty();
        }
    }
}
