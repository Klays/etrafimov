package com.training.service.impl;

import com.training.dao.impl.GoodsDaoIml;
import com.training.form.OrderForm;
import com.training.model.Good;
import com.training.service.GoodService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class GoodServiceImpl implements GoodService {

    private final GoodsDaoIml goodsDao;

    @Override
    public List<Good> findAll() {
        return goodsDao.getAll();
    }

    @Override
    public Optional<Good> findById(Long id) {
        return Optional.ofNullable(goodsDao.getById(id));
    }

    @Override
    public List<Good> addGoodToCart(OrderForm orderForm, List<Good> goods) {
        if (goods == null) {
            goods = new ArrayList<>();
        }
        Optional<Good> good = findById(orderForm.getGoodId());
        if (good.isPresent()) {
            goods.add(good.get());
        }
        return goods;
    }

    @Override
    public BigDecimal getTotalPrice(List<Good> goods) {

        if (goods != null) {
            return goods.stream().map(Good::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add);
        } else {
            return BigDecimal.ZERO;
        }
    }

    @Override
    public List<Good> getUserOrders(String login) {
        return goodsDao.getUserOrders(login);
    }
}
