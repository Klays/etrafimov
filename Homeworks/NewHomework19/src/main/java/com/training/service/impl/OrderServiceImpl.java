package com.training.service.impl;

import com.training.dao.impl.OrdersDao;
import com.training.model.Good;
import com.training.model.User;
import com.training.service.GoodService;
import com.training.service.OrderService;
import com.training.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrdersDao ordersDao;
    private final UserService userService;
    private final GoodService goodService;


    @Override
    public Long insertOrder(Long userId, BigDecimal totalPrice) {
        return ordersDao.insertOrder(userId, totalPrice);
    }

    @Override
    public Long insertOrderGoods(Long orderId, Long goodId) {
        return ordersDao.insertOrderGoods(orderId, goodId);
    }

    @Override
    public void submitOrder(String login, List<Good> goods) {
        Optional<User> user = userService.findByLogin(login);
        Long orderId = insertOrder(user.get().getId(), goodService.getTotalPrice(goods));
        goods.forEach(el -> insertOrderGoods(orderId, el.getId()));
    }

}
