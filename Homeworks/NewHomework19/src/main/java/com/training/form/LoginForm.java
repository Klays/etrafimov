package com.training.form;

import lombok.Data;

@Data
public class LoginForm {

    private String login;
}
