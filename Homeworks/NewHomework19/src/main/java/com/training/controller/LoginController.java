package com.training.controller;

import com.training.form.LoginForm;
import com.training.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequiredArgsConstructor
@SessionAttributes("login")
public class LoginController {

    private final UserService userService;

    @GetMapping("/login")
    public ModelAndView loginPage() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("loginForm", new LoginForm());
        modelAndView.setViewName("index");
        return modelAndView;
    }

    @PostMapping("/login")
    public String login(@ModelAttribute("loginForm") LoginForm loginForm,
                        @RequestParam(value = "terms", required = false) String checkboxValue,
                        Model model) {
        if (checkboxValue != null) {
            model.addAttribute("login", loginForm.getLogin());
            userService.login(loginForm.getLogin());
            return "redirect:/goods";
        } else {
            return "termsErrorPage";
        }
    }

    @GetMapping({"", "/"})
    public String showHomePage() {
        return "redirect:/login";
    }

}
