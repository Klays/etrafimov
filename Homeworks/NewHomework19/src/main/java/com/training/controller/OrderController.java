package com.training.controller;

import com.training.model.Good;
import com.training.service.GoodService;
import com.training.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import java.util.ArrayList;
import java.util.List;

import static com.training.controller.GoodController.ORDERED_GOODS;

@Controller
@RequiredArgsConstructor
@SessionAttributes({"login", "orderedGoods"})
public class OrderController {

    private final GoodService goodService;
    private final OrderService orderService;

    @PostMapping("/orders")
    public String submitOrder(Model model) {
        String login = (String) model.getAttribute("login");
        List<Good> goods = (ArrayList<Good>) model.getAttribute(ORDERED_GOODS);
        orderService.submitOrder(login, goods);

        return "redirect:/orders";
    }

    @GetMapping("/orders")
    public String showOrder(Model model) {
        String login = (String) model.getAttribute("login");
        List<Good> goodList = goodService.getUserOrders(login);
        model.addAttribute("orders", goodList);
        model.addAttribute("totalPrice", goodService.getTotalPrice(goodList));
        return "orderPage";
    }


}
