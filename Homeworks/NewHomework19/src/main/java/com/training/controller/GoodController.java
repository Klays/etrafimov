package com.training.controller;

import com.training.form.OrderForm;
import com.training.model.Good;
import com.training.service.GoodService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequiredArgsConstructor
@SessionAttributes({"login", "orderedGoods"})
public class GoodController {

    private final GoodService goodService;

    public static final String ORDERED_GOODS = "orderedGoods";

    @GetMapping("/goods")
    public ModelAndView goods(Model model) {
        ModelAndView modelAndView = new ModelAndView("productsPage");
        List<Good> goods = (ArrayList<Good>) model.getAttribute(ORDERED_GOODS);
        modelAndView.addObject("totalPrice", goodService.getTotalPrice(goods));
        modelAndView.addObject("orderForm", new OrderForm());
        modelAndView.addObject("goods", goodService.findAll());
        return modelAndView;
    }

    @PostMapping("/goods")
    public String goodsCatalog(@ModelAttribute("loginForm") OrderForm orderForm,
                               Model model) {

        List<Good> goods = (ArrayList<Good>) model.getAttribute(ORDERED_GOODS);
        model.addAttribute(ORDERED_GOODS, goodService.addGoodToCart(orderForm, goods));
        return "redirect:/goods";

    }
}

