package com.training.Task;

import java.util.Map;

public class OperationSystem {
    public static Folder rootFolder = new Folder("root", 0);

    private OperationSystem() {
    }

    public static void print(Folder folder) {
        System.out.println(folder);
        for (Map.Entry<String, Folder> entry : folder.getFolders().entrySet()
        ) {
            print(entry.getValue());
        }
        for (File file : folder.getFiles()
        ) {
            System.out.println(file);
        }
    }

    public static String tab(int tabCounter) {
        StringBuilder tab = new StringBuilder();
        for (int i = 0; i < tabCounter; i++) {
            tab.append(" ");
        }
        return tab.toString();
    }
}
