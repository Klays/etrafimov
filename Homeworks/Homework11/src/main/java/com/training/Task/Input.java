package com.training.Task;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

import static com.training.Task.OperationSystem.rootFolder;

public class Input {

    private static final String filePath = "D:\\kek\\file.txt";

    public void enterFromTheKeyboard() {

        Input input = new Input();

        PathProcessor processor = new PathProcessor();
        Scanner scanner = new Scanner(System.in);

        while (true) {
            String path = scanner.next();
            if (path.equals("STOP")) {
                System.exit(0);
            } else if (path.equals("Write")) {
                input.WriteObjectToFile(filePath, rootFolder);
            } else if (path.equals("Read")) {
                rootFolder = (Folder) input.ReadObjectFromFile(filePath);
            } else if (path.equals("Print")) {
                OperationSystem.print(rootFolder);
                continue;
            } else {
                processor.process(path);
            }
        }
    }

    public void WriteObjectToFile(String filepath, Object serObj) {

        try {

            FileOutputStream fileOut = new FileOutputStream(filepath);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(serObj);
            objectOut.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public Object ReadObjectFromFile(String filepath) {

        try {

            FileInputStream fileIn = new FileInputStream(filepath);
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);

            Object obj = objectIn.readObject();

            objectIn.close();
            return obj;

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}