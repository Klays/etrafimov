package com.training.Task;

public class FileSystem {

    protected String name;

    protected int tabCounter;

    public int getTabCounter() {
        return tabCounter;
    }

    public void setTabCounter(int tabCounter) {
        this.tabCounter = tabCounter;
    }

    public FileSystem() {
    }

    public FileSystem(String name, int tabCounter) {
        this.name = name;
        this.tabCounter = tabCounter;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}