package Task;

import com.training.Task.PathProcessor;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PathProcessorTest {

    @Test
    void testProcess() {
        PathProcessor processor = new PathProcessor();
        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> processor.process(null)
        );
        assertTrue(thrown.getMessage().contains("Path is null"));
    }

    @Test
    void testValidatePath() {
        PathProcessor processor = new PathProcessor();
        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> processor.process("root/file.txt/folder")
        );
        assertTrue(thrown.getMessage().contains("file cannot be added to file or folder"));
    }

}
