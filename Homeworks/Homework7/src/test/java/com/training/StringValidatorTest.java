package com.training;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class StringValidatorTest {

    @Test
    void testValidateSmallLetter() {
        StringValidator stringValidator = new StringValidator();
        ValidateException thrown = assertThrows(
                ValidateException.class,
                () -> stringValidator.validate("kek")
        );
        assertTrue(thrown.getMessage().contains("he word does not start with a capital letter"));
    }
}