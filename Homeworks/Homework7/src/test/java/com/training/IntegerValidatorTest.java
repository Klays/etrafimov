package com.training;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class IntegerValidatorTest {

    @Test
    void testValidateOutOfRange() {
        IntegerValidator integerValidator = new IntegerValidator();
        ValidateException thrown = assertThrows(
                ValidateException.class,
                () -> integerValidator.validate(11)
        );
        assertTrue(thrown.getMessage().contains("The number does not belong to the interval [0-10]"));
    }
}