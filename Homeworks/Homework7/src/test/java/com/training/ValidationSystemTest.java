package com.training;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ValidationSystemTest {

    @Test
    void testChoosingValidatorNull() {
        ValidationSystem validationSystem = new ValidationSystem();
        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> validationSystem.validate(null)
        );
        assertTrue(thrown.getMessage().contains("Object is null"));
    }

    @Test
    public void testValidateInt() {
        ValidationSystem validationSystem = new ValidationSystem();
        validationSystem.validate(1);
        validationSystem.validate(5);
        validationSystem.validate(10);
    }

    @Test
    public void testValidateIntFail() {
        ValidationSystem validationSystem = new ValidationSystem();
        ValidateException thrown = assertThrows(
                ValidateException.class,
                () -> validationSystem.validate(11)
        );
        assertTrue(thrown.getMessage().contains("The number does not belong to the interval [0-10]"));
    }

    @Test
    public void testValidateIntFails2() {
        ValidationSystem validationSystem = new ValidationSystem();
        validationSystem.validate(0);
    }

    @Test
    public void testValidateString() {
        ValidationSystem validationSystem = new ValidationSystem();
        validationSystem.validate("Hello");
        validationSystem.validate("Hello world, abc");
    }

    @Test
    public void testValidateStringFails() throws ValidateException {
        ValidationSystem validationSystem = new ValidationSystem();
        ValidateException thrown = assertThrows(
                ValidateException.class,
                () -> validationSystem.validate("hello")
        );
        assertTrue(thrown.getMessage().contains("The word does not start with a capital letter"));
    }

    @Test
    public void testValidateStringFails2() throws ValidateException {
        ValidationSystem validationSystem = new ValidationSystem();
        ValidateException thrown = assertThrows(
                ValidateException.class,
                () -> validationSystem.validate("")
        );
        assertTrue(thrown.getMessage().contains("The word does not start with a capital letter"));
    }

}