package com.training;

public class IntegerValidator implements Validator<Integer> {

    @Override
    public void validate(Integer number) {
        if (number > 10 || number < 0) {
            throw new ValidateException("The number does not belong to the interval [0-10]");
        }
    }
}
