package com.training;

public interface Validator<T> {
    void validate(T t);
}
