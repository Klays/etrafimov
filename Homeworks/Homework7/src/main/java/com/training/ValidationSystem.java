package com.training;

import java.util.HashMap;
import java.util.Map;

public class ValidationSystem {

    private final Map<Class<?>, Validator> validators = initializeValidators();

    private Map<Class<?>, Validator> initializeValidators() {
        Map<Class<?>, Validator> map = new HashMap();
        map.put(String.class, new StringValidator());
        map.put(Integer.class, new IntegerValidator());
        return map;
    }

    public void validate(Object object) {
        if (object == null) {
            throw new IllegalArgumentException("Object is null");
        }
        validators.get(object.getClass()).validate(object);

    }
}

