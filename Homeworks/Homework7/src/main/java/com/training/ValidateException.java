package com.training;

public class ValidateException extends RuntimeException {

    public ValidateException(String string){
        super(string);
    }
}
