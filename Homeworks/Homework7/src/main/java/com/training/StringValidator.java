package com.training;

public class StringValidator implements Validator<String> {

    public static final String FIRST_CAPITAL_REGEX = "^[A-Z].*";

    @Override
    public void validate(String word) {
        boolean isTrue = word.matches(FIRST_CAPITAL_REGEX);
        if (!isTrue) {
            throw new ValidateException("The word does not start with a capital letter");
        }
    }
}
